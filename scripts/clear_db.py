'''
Utility script used when developing to quickly clear the local database
so it is nice and clean for e.g. a new simmulation or to quickly get rid
of incorrect state.
'''
from datetime import datetime, timedelta

from backend.framework.models import make_tables, RunClass, RunClassAlias

from backend.framework.database import local_db as db


def main():

    make_tables()


    with db.connect():

        session = db.session

        print 'Adding run classes'

        session.add( RunClass( name='Collisions18'    ))
        session.add( RunClass( name='Commissioning18' ))
        session.add( RunClass( name='Cosmics18'       ))

        session.add( RunClassAlias( run_class='Collisions18', alias='Collisions18%'  ))
        session.add( RunClassAlias( run_class='Cosmics18',    alias='Cosmics18%'     ))


        session.commit()

        print 'Done'



if __name__ == '__main__':
    main()
