#!/usr/bin/env python
'''
    Starts the flask application
'''


def main():
    
    from app import create_app
    app = create_app()

    app.logger.info('Starting app')
    context = ('/data/secrets/key.crt', '/data/secrets/key.key')
    app.run(host=app.config['HOST'], port=app.config['PORT'], debug=app.config['DEBUG'], threaded=True, ssl_context=context)



if __name__ == '__main__':
    main()
