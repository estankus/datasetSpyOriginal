from backend.apis.rr_api import RRApi
from backend.apis.web_api import WebApi
from datetime import datetime, timedelta

'''
Script to determine if it's possible, that we retrieve a run but
stream config is still empty
Run via crontab
Also have to check later if config actually appeared, because some runs just don't have it
'''

rrapi = RRApi().wrapped
webapi = WebApi().wrapped

runs = rrapi.get_runs( min_start = datetime.now() - timedelta( hours = 1 ) )

for r in runs:
    config = webapi.get_stream_config( r['number'] )

    if not config:
        print r['number']

