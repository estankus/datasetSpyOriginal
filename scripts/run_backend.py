import logging

from backend.framework.agency               import Agency

from backend.apis.web_api                   import WebApi
from backend.apis.rr_api                    import RRApi
from backend.apis.utils_api                 import UtilsApi

from backend.agents.cms_run_agent           import CMSRunAgent
from backend.agents.config_agent            import ConfigAgent
from backend.agents.dataset_done_agent      import DatasetDoneAgent
from backend.agents.dataset_agent           import DatasetAgent
from backend.agents.file_summaries_agent    import FileSummariesAgent



agency = Agency(

    apis = [
        RRApi(),
        WebApi(),
        UtilsApi(),
    ],

    agents = [ 
        CMSRunAgent(),          # Gets new CMS runs
        ConfigAgent(),          # Gets stream and prompt config data for new runs
        DatasetDoneAgent(),     # Checks if dataset processing has finished (only then we can get that dataset)
        DatasetAgent(),         # Gets necessary datasets for runs and checks for missing datasets
        FileSummariesAgent(),   # Gets count of events and file size for each run-dataset pair
    ]
)

def main():

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument( '-p', '--print_config', action='store_true' )
    parser.add_argument( '-t', '--twiki_format', action='store_true' )

    args = parser.parse_known_args()[0]

    if args.print_config:
        agency.print_configuration( args.twiki_format )
        return


    logger = logging.getLogger( 'backend' )

    formatter = logging.Formatter( '%(levelname)8s: %(message)s' )
    handler   = logging.StreamHandler()
    handler.setFormatter( formatter )

    logger.handlers = []
    logger.addHandler( handler )
    logger.setLevel( logging.INFO )

    agency.run()


if __name__ == '__main__':
    main()
