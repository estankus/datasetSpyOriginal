import sys
import os
import logging
import unittest


modules = {
    'framework'    : 'backend/tests/framework',
    'agents'       : 'backend/tests/agents'
}


def get_modules():

    if len( sys.argv ) < 2 or sys.argv[1] == 'help':
        print_help()
        sys.exit( 0 )
    else:
        module_names = sys.argv[1].split(',')

    module_paths = []
    if module_names == ['all']:
        module_paths += modules.values()
    else:
        for name in module_names:
            if not name in modules:
                print 'Unknown module name: "%s", exiting...' % name
                sys.exit( 2 )
            else:
                module_paths.append( modules[name] )


    return [ unittest.TestLoader().discover( path )
             for path in module_paths ]


def print_help():
    print 'Backend test usage:'
    print 'run_backend_tests testname1,testname2,...'
    print 'Commands:'
    print '  * help    - show this message'
    print '  * all     - test all modules'
    for module in modules:
        pad = ' '*( 8 - len( module ))
        print '  * %s%s- test %s module' % ( module, pad, module )


def main():

    import logging
    logging.basicConfig( level=logging.ERROR )
    
    if not os.getenv( 'DATASETSPY_CONFIG' ) == 'testing':
        logging.error( 'Please make sure that DATASETSPY_CONFIG is set to "testing"' )
        return 1

    
    suite = unittest.TestSuite()

    for module in get_modules():
        suite.addTests( module )


    unittest.TextTestRunner( verbosity=2 ).run( suite )

    return 0




if __name__ == '__main__':
    sys.exit( main() )
