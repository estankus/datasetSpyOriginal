import ConfigParser
import socket
import argparse
import sys
import datetime
import subprocess
import os


class Service:
    

    # Defaults
    name              = 'datasetSpy'
    root_directory    = '/data/services/datasetSpy'
    secrets_directory = '/data/secrets'
    #TODO: determine listening port (probably after finishing backend and starting to work on 'app')
    listening_port    = 8101
    production_level  = 'private'
    caches            = None

    hostname          = socket.gethostname()
    
    _git_info         = None
    _secrets          = None


    def __init__( self ):

        self.START_TIME = datetime.datetime.utcnow()

        if os.getenv( 'DATASETSPY_CONFIG' ) == 'testing':
            self.production_level = 'testing'
        else:
            
            arguments = self._parseargs()

            self.name               = arguments.name             or Service.name
            self.root_directory     = arguments.rootDirectory    or Service.root_directory
            self.secrets_directory  = arguments.secretsDirectory or Service.secrets_directory
            self.listening_port     = arguments.listeningPort    or Service.listening_port
            self.production_level   = arguments.productionLevel  or Service.production_level
            self.caches             = arguments.caches           or Service.caches


    @property
    def git_info( self ):
        if not self._git_info: self._load_git_info()
        return self._git_info
    

    @property
    def host_info( self ):
        return { 'hostname': self.hostname, 'port' : self.listening_port }    


    @property
    def secrets( self ):
        if not self._secrets: self._load_secrets()
        return self._secrets

    @property
    def production_level_nice( self ):
        return {
            'dev'     : 'Development',
            'int'     : 'Integration',
            'pro'     : 'Production',
            'testing' : 'Testing'
        }.get( self.production_level, 'Private' )
      



    def get_config( self, config_dir ):

        default_file  = 'default.cfg'
        specific_files = {
            'dev'     : 'dev.cfg',
            'int'     : 'int.cfg',
            'pro'     : 'prod.cfg',
            'testing' : 'testing.cfg'
        }
        specific_file = specific_files.get( self.production_level, 'private.cfg' )

        default_filename  = os.path.join( config_dir, default_file )
        specific_filename = os.path.join( config_dir, specific_file )

        cfg = ConfigParser.ConfigParser()
        cfg.optionxform = str
        cfg.read( [default_filename, specific_filename] )

        return cfg


    def read_connection( self, cfg, section ):
        ''' 
            Build db connection string. Possibly by using secrets.
        '''
        template = cfg.get( section, 'conn_string' )

        # If no secrets are involved
        if not ( cfg.has_option( section, 'secrets_location') and 
                 cfg.has_option( section, 'secrets_fields')):
            
            # If just the name of a sqlite file -> Make abs path to sqlite file
            if template.startswith( 'sqlite:///' ):

                dir_path = os.path.dirname( os.path.abspath( __file__ ))
                filename = os.path.basename( template )
                
                return os.path.join( 'sqlite:///%s'%dir_path, filename )

            # If some other, weird name -> return it
            else:
                return template


        # Build db connection with secrets fields
        location = cfg.get( section, 'secrets_location' ).split(',')
        fields   = cfg.get( section, 'secrets_fields'   ).split(',')

        secrets  = self.get_secrets_subtree( location )
        return template % tuple( secrets.get( f ) for f in fields )



    def get_secrets_subtree( self, path ):
        '''
        Returns a subtree of secrets, defined by :path:
        '''
        if type( path ) == str:
            path = path.split( ',' )
        assert type( path ) == list

        secrets = self.secrets
        for field in path:
            if not field in path:
                raise ValueError( 'Path "%s" does not exist in secrets' % path )

            secrets = secrets[ field ]

        return secrets


    def get_secrets_file( self, filename ):
        '''
        Returns the absolute path to a file that
        resides in the secrets directory
        '''
        abs_path = os.path.join( self.secrets_directory, filename )
        if os.path.isfile( abs_path ):
            return abs_path
        else:
            raise ValueError( 'Secrets file "%s" does not exist'%filename )




    def _load_git_info( self ):
        ''' 
            Attempts to retrieve git info
        '''
        try:
            tag = subprocess.check_output(["git", "describe", "--abbrev=0"]).strip(),
        except:
            tag = 'N/A'
        try:
            commit = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).strip()
        except:
            commit = 'N/A'
        
        self._git_info =  { 'tag': tag, 'commit': commit }



    def _load_secrets( self ):
        '''
            Attempts to retrieve secrets
        '''
        sys.path.append( self.secrets_directory )
        import secrets
        self._secrets = secrets.secrets



    def _parseargs( self ):
        '''
            Parse args
        '''
        parser = argparse.ArgumentParser()

        parser.add_argument('-n', '--name', nargs='?', type = str,
            dest  = 'name',
            help  = 'The name of the service.',
        )

        parser.add_argument('-r', '--rootDirectory', nargs='?', type = str,
            dest  = 'rootDirectory',
            help  = 'The root directory for the service.',
        )

        parser.add_argument('-s', '--secretsDirectory', nargs='?', type = str,
            dest  = 'secretsDirectory',
            help  = 'The shared secrets directory.',
        )

        parser.add_argument('-p', '--listeningPort', nargs='?', type = int,
            dest  = 'listeningPort',
            help  = 'The port this service will listen to.',
        )

        parser.add_argument('-l', '--productionLevel', nargs='?', type = str,
            dest  = 'productionLevel',
            help  = 'The production level this service should run as, which can be one of the following: "dev" == Development, "int" == Integration, "pro" == Production. For instance, the service should use this parameter to decide to which database connect, to which mailing list should send emails, etc.',
        )

        parser.add_argument('-c', '--caches', nargs='?', type = str,
            dest  = 'caches',
            help  = 'The cache to ID mapping of the caches that the service uses.',
        )

        return parser.parse_known_args()[0]



service = Service()
