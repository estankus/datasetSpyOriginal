export class DatasetEvents {
    constructor(
      public run: number,
      public num_event: number,
      public run_start: Date,
      public cumm_events: number,
    ) { }
  }