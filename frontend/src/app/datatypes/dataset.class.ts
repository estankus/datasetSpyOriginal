export class Dataset{
    constructor(
        public name: string,
        public primary_ds_name: string,
        public acquisition_era: string,
        public data_tier: string
      ) { }
}