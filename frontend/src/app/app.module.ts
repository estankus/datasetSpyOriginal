//Angular modules
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'


//External modules
import { NgxEchartsModule } from 'ngx-echarts'
import 
{  
  NgbAlertModule,
  NgbDropdownModule,
  NgbButtonsModule
} 
from '@ng-bootstrap/ng-bootstrap'


//Components
import { AppComponent } from './app.component'
import { PageHomeComponent } from './page.home/page_home.component'
import { PageChartsComponent } from './page.charts/page_charts.component'
import { SelectDatasetComponent } from './page.charts/select_dataset.component'
import { SelectRunClassComponent } from './page.charts/select_run_class.component'
import { EventChartWrapperComponent } from './charts/event_chart_wrapper.component'
import { EventChartComponent } from './charts/event_chart.component'
import { DropdownComponent } from './widgets/dropdown.component'
import { NavbarComponent } from './widgets/navbar.component'
import { SidebarComponent } from './widgets/sidebar.component'


//Services
import { ApiService } from './services/api.service'
import { RoutingService } from './services/routing.service'


const appRoutes: Routes = [
  { 
    path:'home',
    component: PageHomeComponent
  },
  {
    path:'charts',
    component: PageChartsComponent
  },
  { 
    path: '',
    redirectTo: 'home',
    pathMatch: 'full' 
  },
  { 
    path: '**', 
    redirectTo: 'home' 
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PageHomeComponent,
    PageChartsComponent,
    SelectDatasetComponent,
    SelectRunClassComponent,
    EventChartWrapperComponent,
    EventChartComponent,
    DropdownComponent,
    NavbarComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    NgxEchartsModule,
    NgbAlertModule,
    NgbDropdownModule,
    NgbButtonsModule
  ],
  providers: [
    ApiService,
    RoutingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
