import { environment } from '../environments/environment'

let apiPrefix = environment.api_url

export class NAV {
    static readonly HOME = 'home'
    static readonly CHARTS = 'charts'
    static readonly ADMIN = 'admin'
}

export class API {
    static readonly DATASETS = `${apiPrefix}datasets`
    static readonly DATASET_EVENTS  = `${apiPrefix}dataset_events`
    static readonly RUN_CLASSES = `${apiPrefix}run_classes`
}

class LINKS {

    static readonly TWIKI_END_USERS  = 'https://twiki.cern.ch/twiki/bin/view/CMS/InstructionsForDatasetSpyUsage'
    static readonly TWIKI_DEVELOPERS  = 'https://twiki.cern.ch/twiki/bin/view/CMS/InstructionsForDatasetSpyDevelopment'
    static readonly JIRA_PROJECT      = 'http://NOT_AVAILABLE'
  }
  

class EMAIL {

    static readonly DEVS    = 'cms-cond-dev@cern.ch';
    static readonly EXPERTS = 'cms-offlinedb-exp@cern.ch'
  
  }
  

export class URLS {

    static readonly NAV   = NAV
    static readonly API   = API;
    static readonly LINKS = LINKS;
    static readonly EMAIL = EMAIL;
  
  }
  

