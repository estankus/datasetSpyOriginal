import {Component} from '@angular/core';

import {URLS} from '../app.urls'

@Component({
    selector: 'app-page-home',
    templateUrl: './page_home.component.html',
  })
export class PageHomeComponent{

    // To use URLS in template
    URLS = URLS
}