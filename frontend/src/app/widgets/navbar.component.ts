import { Component } from '@angular/core';

import { ApiService } from '../services/api.service';
import {URLS} from '../app.urls'


@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
})
export class NavbarComponent {

    // To use URLS in template
    URLS = URLS
    // Navbar in mobile
    isCollapsed = true;

    requestProblems : boolean;
    alertClosed = false

    constructor( private apiService  : ApiService ) {}


    ngOnInit() : void {
        this.apiService.requestProblems
        .subscribe( problem => this.requestProblems = problem );
    }

    tryLogin() : void {
        window.location.reload();
    }

}
