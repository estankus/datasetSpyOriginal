import { Component, Input, Output, EventEmitter } from '@angular/core'


@Component({
  selector: 'app-dropdown',
  template: `
  <div ngbDropdown placement="{{droplistPlacement}}">

    <button ngbDropdownToggle   [ngClass]="[classes, selected ? 'btn-outline-primary' : 'btn-outline-secondary']" 
                                class="btn mb-2" 
                                style="width:18rem; white-space: normal;">
        {{ selected ? selected.name || selected : emptyOption ? emptyOptionMsg : promptMsg }}
    </button>

    <div ngbDropdownMenu style="max-height: 40rem; overflow-y: scroll;">
        <button class="dropdown-item" 
                *ngIf="emptyOption"
                (click)="onSelect.next(undefined)">
            {{emptyOptionMsg}}
        </button>

        <div class="dropdown-divider" *ngIf="emptyOption"></div>

        <button class="dropdown-item" disabled
                *ngIf="!options || options.length === 0">
            {{emptyMsg}}
        </button>

        <button class="dropdown-item" 
                *ngFor="let option of options"
                (click)="onSelect.next(option)">
            {{option.name || option}}
        </button>
    </div>
  </div>
  `,
})
export class DropdownComponent {

  @Input() options      : any[]
  @Input() selected     : any
  //Whether to have selectable option which returns 'undefined'
  @Input() emptyOption    : boolean = false
  @Input() emptyOptionMsg : string = 'Empty Option'
  @Input() emptyMsg     : string  = 'No options'
  @Input() promptMsg    : string  = 'No option selected'
  @Input() classes      : string  = 'btn-outline-primary'
  @Input() droplistPlacement : string = 'bottom-left'

  @Output()
  onSelect : EventEmitter<any> = new EventEmitter<any>()

}
