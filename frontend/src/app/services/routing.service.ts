import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';

import {Observable} from 'rxjs/Observable'


@Injectable()
export class RoutingService {

    constructor(private router : Router,
                private activatedRoute : ActivatedRoute) {}

    getQueryParamMap() : Observable<ParamMap> {
        return this.activatedRoute.queryParamMap
    }

    getQueryParams() : any {
        return this.activatedRoute.snapshot.queryParams;
      }    
    
    setQueryParams( params:Params, replaceState=false ) : void {
        this.router.navigate( [], { 
            queryParams : params,
            replaceUrl  : replaceState
        })
    }

    updateQueryParams( params:Params, replaceState=false ) : void {
        let oldParams = JSON.parse( JSON.stringify( this.getQueryParams() ))
        let newParams = Object.assign( oldParams, params )
        this.setQueryParams( newParams, replaceState )
    }
                     
}
