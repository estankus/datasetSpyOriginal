import {Injectable} from '@angular/core'
import {HttpClient, HttpErrorResponse} from '@angular/common/http'

import {Observable} from 'rxjs/Observable'
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/map';

import {URLS} from '../app.urls'
import {Dataset} from '../datatypes/dataset.class'
import {DatasetEvents} from '../datatypes/dataset_events.class'
import {RunClass} from '../datatypes/run_class.class'

@Injectable()
export class ApiService {   

	requestProblems : Subject<boolean> = new Subject<boolean>()

	constructor(private http: HttpClient) {
	}

	private handleError(err: HttpErrorResponse | any) {

		console.log( 'Error - Unable to connect to datasetSpy API' );

		this.requestProblems.next( true )

		return Observable.throw(err.message || 'Error: Unable to connect to datasetSpy API.');
	}

	/*
	Story time:
	Those get methods were typed (: Observable<SomeClass[]>), but one day I changed something else
	and it started throwing errors about type:
	Type 'Observable<{} | DatasetEvents[]>' is not assignable to type 'Observable<DatasetEvents[]>'

	I'm not an expert, so for now their type is any
	*/

	getDatasetEvents( dataset: string, runClass: string ): any {
		let url = `${URLS.API.DATASET_EVENTS}?dataset=${dataset}`

		if(runClass != "") {
			url += `&run_class=${runClass}`
		}

		return this.http
			.get<DatasetEvents[]>(url)
			.map(x => {
				for(let d of x) {
					d.run_start = new Date(d.run_start)
				}

				return x
			})
			.catch(this.handleError.bind(this))
	}

	getDatasets(): any {
	    return this.http
			.get<Dataset[]>(`${URLS.API.DATASETS}`)
			.catch(this.handleError.bind(this));
	}

	getRunClasses(): any {
		return this.http
			.get<RunClass[]>(`${URLS.API.RUN_CLASSES}`)
			.catch(this.handleError.bind(this));
	}
}