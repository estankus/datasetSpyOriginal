import {Component, OnInit} from '@angular/core';

import {Dataset} from '../datatypes/dataset.class'
import {ApiService} from '../services/api.service';
import {RoutingService} from '../services/routing.service';

@Component({
    selector: 'select-dataset',
    templateUrl: './select_dataset.component.html'
  })
export class SelectDatasetComponent implements OnInit{

        // Array received from api
        datasets: Dataset[]
        // Datasets filtered by selected criteria
        filteredDatasets: Dataset[]
        
        // Arrays with unique entries
        primaryDsNames: string[]
        acquisitionEras: string[]
        dataTiers: string[]

        // Selected values in dropdown menus
        selectedPrimaryDsName: string = undefined
        selectedAcquisitionEra: string = undefined
        selectedDataTier: string = undefined
        selectedDataset: Dataset = undefined

        constructor(private apiService: ApiService, 
                    private routingService: RoutingService) {
        }

        ngOnInit() {
            let subscription = this.apiService
            .getDatasets()
            .subscribe(x => {
                this.writeUniqueEntries(x)

                this.routingService.getQueryParamMap().subscribe(params => {
                    this.selectedDataset = this.datasets.find(
                        x => x.name === params.get("dataset")
                    )
                })

                subscription.unsubscribe()
                }, console.error
            );
            
        }

        writeUniqueEntries(datasets: Dataset[]) {
            this.datasets = datasets.sort((a, b) => a.name.localeCompare(b.name))

            this.primaryDsNames = datasets.map(x => x.primary_ds_name)
            this.primaryDsNames = Array.from(new Set(this.primaryDsNames)).sort()

            this.acquisitionEras = datasets.map(x => x.acquisition_era)
            this.acquisitionEras = Array.from(new Set(this.acquisitionEras)).sort()

            this.dataTiers = datasets.map(x => x.data_tier)
            this.dataTiers = Array.from(new Set(this.dataTiers)).sort()

            this.filterDatasets()
        }

        onPrimaryDsNameSelect(selected:string) {
            this.selectedPrimaryDsName = selected
            this.filterDatasets()
        }

        onAcquisitionEraSelect(selected:string) {
            this.selectedAcquisitionEra = selected
            this.filterDatasets()
        }

        onDataTierSelect(selected:string) {
            this.selectedDataTier = selected
            this.filterDatasets()
        }

        onDatasetSelect(selected:Dataset) {
            this.selectedDataset = selected
            this.routingService.updateQueryParams({dataset:this.selectedDataset ? this.selectedDataset.name : undefined})
        }

        filterDatasets() {
            this.filteredDatasets = this.datasets

            if(this.selectedPrimaryDsName) {
                this.filteredDatasets = this.filteredDatasets.filter
                    (x => x.primary_ds_name === this.selectedPrimaryDsName)
            }

            if(this.selectedAcquisitionEra) {
                this.filteredDatasets = this.filteredDatasets.filter
                    (x => x.acquisition_era === this.selectedAcquisitionEra)
            }

            if(this.selectedDataTier) {
                this.filteredDatasets = this.filteredDatasets.filter
                    (x => x.data_tier === this.selectedDataTier)
            }

            // Following logic controls the selected dataset in the dropdown when filtering parameters change
            // (Change selected dataset if after filtering previously selected dataset disappears from the dropdown)

            // On fresh page load we want things to be empty
            if(this.selectedPrimaryDsName || this.selectedAcquisitionEra || this.selectedDataTier ) {
                this.selectedDataset = this.filteredDatasets.find(x => x === this.selectedDataset)

                if(this.selectedDataset === undefined) {
                    this.selectedDataset = this.filteredDatasets[0]
                    // Because we changed selected dataset have to update queryParams
                    this.onDatasetSelect(this.selectedDataset)
                }
            }
        }
}