import {Component, OnInit} from '@angular/core';

import {ApiService} from '../services/api.service';
import {RoutingService} from '../services/routing.service';
import {RunClass} from '../datatypes/run_class.class'


@Component({
    selector: 'select-run_class',
    template: `
    <label class="text-muted mb-0" for="selectRunClass">Run class</label>
    <app-dropdown   id="selectRunClass"
                    [options]="runClasses"
                    [selected]="selectedRunClass"
                    [emptyOption]="true"
                    [emptyOptionMsg]="'Any'"
                    (onSelect)="onRunClassSelect($event)">
    </app-dropdown>

    `
  })
export class SelectRunClassComponent implements OnInit{

    runClasses:RunClass[]
    selectedRunClass:RunClass = undefined
    
    constructor(private apiService: ApiService,
                private routingService: RoutingService) {
    }

    ngOnInit() {
        let subscription = this.apiService
        .getRunClasses()
        .subscribe(x => {
            this.runClasses = x

            this.routingService.getQueryParamMap().subscribe(params => {
                this.selectedRunClass = this.runClasses.find(
                    x => x.name === params.get("run_class")
                )
            })

            subscription.unsubscribe()
        }, console.error)
    }

    onRunClassSelect(selected:RunClass) {
        this.selectedRunClass = selected
        this.routingService.updateQueryParams({run_class:this.selectedRunClass ? this.selectedRunClass.name : undefined})
    }

}