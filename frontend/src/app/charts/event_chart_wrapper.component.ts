import {Component, Input} from '@angular/core';

import {ApiService} from '../services/api.service';
import {Dataset} from '../datatypes/dataset.class'
import {DatasetEvents} from '../datatypes/dataset_events.class'
import {RunClass} from '../datatypes/run_class.class'

@Component({
    selector: 'event-chart-wrapper',
    template: `
    <event-chart [datasetEvents]=datasetEvents [showLabels]="showLabels" [timeBased]="timeBased"></event-chart>
    `,
  })
export class EventChartWrapperComponent{
    
    datasetEvents: DatasetEvents[] = []

    // Set externally for making calls to api
    private _dataset: Dataset
    private _runClass: RunClass

    @Input() showLabels: boolean
    @Input() timeBased: boolean

    @Input() 
    set dataset(dataset: Dataset) {  
        this._dataset = dataset
        this.refreshChartData()
    }

    @Input()
    set runClass(runClass: RunClass) {
        this._runClass = runClass
        this.refreshChartData()
    }

    constructor(private apiService: ApiService) {
    }

    refreshChartData() {
        let subscription = this.apiService
            .getDatasetEvents(
                this._dataset == null ? "" : this._dataset.name, 
                this._runClass == null ? "" : this._runClass.name)
            .subscribe(x => {
                x.sort((a, b) => a.run - b.run) // Just in case

                this.calculateCummulative(x)
                this.datasetEvents = x

                subscription.unsubscribe()
            }, console.error)
    }

    calculateCummulative(datasetEvents:DatasetEvents[]) {
        let events = 0

        datasetEvents.forEach(x => {          
            x.cumm_events = events + x.num_event
            events += x.num_event
        })
    }


}