import {Component, Input} from '@angular/core';

import {DatasetEvents} from '../datatypes/dataset_events.class'

@Component({
        selector: 'event-chart',
        template: `
        <div echarts [options]="options" [merge]="merge" style="height:90%; width:100%"></div>
        `,
    })
export class EventChartComponent{

    @Input() 
    set datasetEvents(datasetEvents: DatasetEvents[]) {
        let merge = Object.assign({}, this.merge)

        if(this.merge.dataset[0].source.length == datasetEvents.length) {
            // Comparable datasets, keep the zoom
            delete merge.dataZoom[0].start
            delete merge.dataZoom[0].end
        } else {
            // Different datasets, unzoom
            merge.dataZoom[0].start = 0
            merge.dataZoom[0].end = 100
        }
        
        if (datasetEvents.length) {
            merge.dataset[0].source = datasetEvents.map(x => [x.num_event, x.run_start, x.run])
            merge.dataset[1].source = datasetEvents.map(x => [x.cumm_events, x.run_start, x.run])
        } else {
            // Throws error if not set
            merge.dataset[0].source = [[]]
            merge.dataset[1].source = [[]]
        }


        this.merge = merge
    }

    @Input()
    set showLabels(showLabels: boolean) {
        let merge = Object.assign({}, this.merge)

        merge.series[0].label.show = showLabels
        // Want to keep the zoom
        delete merge.dataZoom[0].start
        delete merge.dataZoom[0].end

        this.merge = merge
    }

    @Input()
    set timeBased(timeBased: boolean) {
        let merge = Object.assign({}, this.merge)

        if(timeBased) {  
            merge.xAxis.type = "time"
            // Bar width doesn't adapt with time axis and 'empty' filter
            merge.dataZoom[0].filterMode = "weakFilter"
            merge.dataZoom[1].filterMode = "weakFilter"
            // Map time to x axis (see datasetEvents())
            merge.series[0].encode = {x:1, y:0}
            merge.series[1].encode = {x:1, y:0}
        } else {
            merge.xAxis.type = "category"
            merge.dataZoom[0].filterMode = "empty"
            merge.dataZoom[1].filterMode = "empty"
            // Map run number to x axis (see datasetEvents())
            merge.series[0].encode = {x:2, y:0}
            merge.series[1].encode = {x:2, y:0}
        }

        // Unzoom chart
        merge.dataZoom[0].start = 0
        merge.dataZoom[0].end = 100

        this.merge = merge
    }

    //Chart configuration
    options = {
        animation:false,
        tooltip : {
            trigger:'axis',
            formatter: (params) => {
                return `
                <b>Run ${params[0].value[2]}</b><br/>
                ${params[0].marker}Number of events: ${params[0].value[0] !== undefined ?
                    params[0].value[0].toExponential(1) : params[0].value[0]}<br/>
                ${params[1].marker}Cummulative events: ${params[1].value[0] !== undefined ?
                    params[1].value[0].toExponential(1) : params[1].value[0]}<br/>
                Started ${params[0].value[1] ? params[0].value[1].toUTCString() : params[0].value[1]}<br/>
                `
            }
        },
        toolbox: {
            feature: {
                saveAsImage: {
                    title:'save',
                    name:'dataset',
                    iconStyle:{
                        borderColor:"#000000",
                        emphasis:{
                            borderColor:"#FF0000"
                        }
                    }
                }
            }
        },
        grid: {
            left:'1%',
            right:'1%',
            bottom:'50', // Have to fit chart zooming element
            top:'4%',
            containLabel:true
        },
        xAxis :{
                type:'category',
            },
        yAxis : [
            {
                type :'value',
                axisLabel:{
                    formatter: x => {
                        return x.toExponential(1)
                    }
                }
            }, 
            {
                type:'value',
                splitLine:{
                    show:false
                },
                axisLabel:{
                    formatter: x => {
                        return x.toExponential(1)
                    }
                }
            }
        ],
        dataZoom:[
            {
                show:true,
                throttle:0
            },
            {
                type:'inside',
                show:true,
                throttle:0
            }
        ],
        dataset:[
            {
                source:[[]],
                dimensions:["Number of events", "Run start time", "Run number"]
            },
            {
                source:[[]],
                dimensions:["Cummulative events", "Run start time", "Run number"]
            }
        ],
        series : [
            {
                type:'bar',
                name:'Number of events',
                datasetIndex:0,
                label: {
                    position:'top',
                    distance:20,
                    rotate:60,
                    color:"#000000",
                    formatter:x => {
                        return x.value[0].toExponential(1)
                    }
                },
                itemStyle:{
                    color:"#007bff",
                    barBorderColor:"#000000",
                    barBorderWidth:1,
                    opacity:0.8
                },
                barMaxWidth:50
            }, 
            {
                type:'line',
                name:'Cummulative events',
                datasetIndex:1,
                yAxisIndex: 1,
                itemStyle: {
                    color:"#FF4136"
                }
            }
        ]
    }

    // Object for updating data
    merge = {
        xAxis:{
            type:"category"
        },
        dataZoom:[
            {
                filterMode:"empty",
                start:0,
                end:100
            },
            {
                filterMode:"empty",
            }
        ],
        dataset:[
            {
                source:[[]]
            },
            {
                source:[[]]
            }
        ],
        series: [
            {
                encode:{},
                label:{
                    show: false
                }
            },
            {
                encode:{}
            }
        ]
    }
}