import os
import unittest
import tempfile
from datetime import datetime, timedelta

from backend.framework.database import local_db
from backend.framework.models import Base



class BackendTestBase( unittest.TestCase ):
    


    def setUp( self ):

        # Create a fresh db with tempfile
        handle, filename = tempfile.mkstemp()
        self.__database_handle   = handle
        self.__database_filename = filename

        local_db.setUp( 'sqlite:///%s' % filename, False )

        # Make db tables
        Base.metadata.create_all( local_db.engine )
        

    def tearDown( self ):

        # Clean up the db file
        os.close( self.__database_handle )
        os.unlink( self.__database_filename )


    def get_datetimes( self, nof_timestamps ):
        '''
        Returns a list of datetimes with length equal to 
        :nof_timestamps:, spaced one hour apart and
        ending with current utc timestamp
        '''
        now = datetime.utcnow()
        return [ 
            now - timedelta( hours=(nof_timestamps - 1 - i) )
            for i in range( nof_timestamps )
        ]
