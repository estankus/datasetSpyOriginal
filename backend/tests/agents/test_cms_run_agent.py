from datetime import datetime, timedelta

from backend.tests import BackendTestBase
from backend.tests.mock.mock_rr_api import MockRRApi

from backend.framework.database import local_db
from backend.framework.agency import Agency
from backend.framework.models import RunClass, RunClassAlias, CMSRun
from backend.agents.cms_run_agent import CMSRunAgent
from backend.config import config


class TestCMSRunAgent( BackendTestBase ):

    def test_cms_run_agent_get_from_min_start( self ):
        '''
        Testing if agent gets runs from config.MIN_START
        '''

        run_class = 'testRunClass'

        run_1_number = 1
        run_2_number = 2

        before_min_start = config.MIN_START - timedelta( days = 1 )
        after_min_start = config.MIN_START + timedelta ( days = 1 )

        # ---- Setup state
        with local_db.connect():

            local_db.add( RunClass( name=run_class ))

        # ---- Run Agency
        
        rr_api = MockRRApi([{
            'number'       : run_1_number,
            'runClassName' : run_class,
            'startTime'    : before_min_start,
        },{
            'number'       : run_2_number,
            'runClassName' : run_class,
            'startTime'    : after_min_start,
        }])

        rr_api.mock( TIME=datetime.utcnow() )

        Agency(
            apis   = [ rr_api ],
            agents = [ CMSRunAgent() ]
        ).run()

        # ---- Check new state
        
        with local_db.connect():
            
            run1 = ( local_db.query( CMSRun )
                .filter_by( 
                    run_class=run_class,
                    number=run_1_number )
                .first() )

            run2 = ( local_db.query( CMSRun )
                .filter_by( 
                    run_class=run_class,
                    number=run_2_number )
                .first() )

            # First run should not have been fetched as it started before MIN_START 
            assert run1 is None
            # Second run should have been fetched
            assert run2 is not None

    def test_cms_run_agent_get_from_last_run( self ):
        '''
        Testing if agent fetches all runs since last run fetched
        '''

        run_class = 'testRunClass'

        run_1_number = 1
        run_2_number = 2
        run_3_number = 3

        before = datetime.utcnow() - timedelta( days = config.REFETCH_PERIOD + 1 )
        after = datetime.utcnow() - timedelta ( days = config.REFETCH_PERIOD - 1 )

        # ---- Setup state

        with local_db.connect():

            local_db.add( RunClass( name=run_class ))
            local_db.add( CMSRun( number=run_1_number, run_class=run_class,
                start = datetime.utcnow() - timedelta ( days = config.REFETCH_PERIOD + 2 )))

        # ---- Run Agency
        
        rr_api = MockRRApi([{
            'number'       : run_2_number,
            'runClassName' : run_class,
            'startTime'    : before,
        },{
            'number'       : run_3_number,
            'runClassName' : run_class,
            'startTime'    : after,
        }])

        rr_api.mock( TIME=datetime.utcnow() )

        Agency(
            apis   = [ rr_api ],
            agents = [ CMSRunAgent() ]
        ).run()

        # ---- Check new state
        
        with local_db.connect():
            
            run2 = ( local_db.query( CMSRun )
                .filter_by( 
                    run_class=run_class,
                    number=run_2_number )
                .first() )

            run3 = ( local_db.query( CMSRun )
                .filter_by( 
                    run_class=run_class,
                    number=run_3_number )
                .first() )

            # Both runs should be fetched (since they are after last run fetched)
            assert run2 is not None
            assert run3 is not None

    def test_cms_run_agent_refetch( self ):
        '''
        Testing if recent runs are being refetched
        '''

        run_class1 = 'testRunClass1'
        run_class2 = 'testRunClass2'

        run_1_number = 1
        run_2_number = 2


        t1 = datetime.utcnow() - timedelta ( minutes = 10 )
        t2 = datetime.utcnow() - timedelta ( minutes = 5 )
        t3 = datetime.utcnow() + timedelta ( minutes = 5 )

        # ---- Setup state

        with local_db.connect():

            local_db.add( RunClass( name=run_class1 ))
            local_db.add( RunClass( name=run_class2 ))

            local_db.add( CMSRun( number=run_1_number, run_class=run_class1, start = t1 ) )
            local_db.add( CMSRun( number=run_2_number, run_class=run_class2, start = t2 ) )

        # ---- Run Agency
        
        rr_api = MockRRApi([{
            'number'       : run_2_number,
            'runClassName' : run_class2,
            'startTime'    : t2,
        },{
            'number'       : run_1_number,
            'runClassName' : run_class2,
            'startTime'    : t1,
        }])

        rr_api.mock( TIME = t3 )

        Agency(
            apis   = [ rr_api ],
            agents = [ CMSRunAgent() ]
        ).run()

        # ---- Check new state
        
        with local_db.connect():
            
            run1 = ( local_db.query( CMSRun )
                .filter_by( 
                    run_class=run_class1,
                    number=run_1_number )
                .first() )

            run2 = ( local_db.query( CMSRun )
                .filter_by( 
                    run_class=run_class2,
                    number=run_2_number )
                .first() )

            run3 = ( local_db.query( CMSRun )
                .filter_by( 
                    run_class=run_class2,
                    number=run_1_number )
                .first() )

            # run_1_number should have changed its class
            assert run1 is None
            assert run2 is not None
            assert run3 is not None