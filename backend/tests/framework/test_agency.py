from backend.tests import BackendTestBase
from backend.tests.mock.mock_api import TestApi
from backend.tests.mock.mock_agent import TestAgent

from backend.framework.agency import Agency


class TestAgency( BackendTestBase ):

    def test_agency_basic( self ):
        '''
        Tests if agency calls the agent
        '''

        testAgent = TestAgent()

        Agency(
            apis = [ TestApi() ],
            agents = [ testAgent ]
        ).run()

        assert testAgent.agentCalled == True
