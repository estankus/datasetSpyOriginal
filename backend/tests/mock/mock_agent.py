from backend.framework.agent import Agent
from backend.framework import logger

from backend.tests.mock.mock_api import TestApi

class TestAgent ( Agent ):

    def __init__( self ):
        Agent.__init__( self, dependencies=[TestApi] )
        self.agentCalled = False

    def agent( self, test_api ):
        self.agentCalled = True
    