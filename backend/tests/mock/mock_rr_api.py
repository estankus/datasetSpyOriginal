from datetime import datetime
import re


from backend.framework.api import Api
from backend.apis.rr_api import RRApi



@Api( mocking=RRApi )
class MockRRApi:


    # Mockable values
    TIME = datetime.min


    def __init__( self, runs ):
        '''
        Create mock api, runs should be on the format
        {
            number       : number;
            runClassName : string;
            startTime    : :datatime
        }[]
        '''
        
        self.runs = sorted( runs, key=lambda r: r['number'] )



    def get_runs(self,  run_class=None, min_run=None,   max_run=None,
                                        min_start=None, max_start=None ):

        predicates = []

        if run_class: 
            run_class_rgx = re.compile( run_class.replace( '%', '(.)*'). replace( '_', '(.)' ))
            predicates.append( lambda r: run_class_rgx.match( r['runClassName'] ))

        if min_run:
            predicates.append( lambda r: r['number'] >= min_run )

        if max_run:
            predicates.append( lambda r: r['number'] <= max_run )

        if min_start:
            predicates.append( lambda r: r['startTime'] >= min_start )

        if max_start:
            predicates.append( lambda r: r['startTime'] <= max_start )


        result = []
        for run in self.runs:

            run = dict( run.items() )

            # Trim away if run is in the future
            if run['startTime'] > self.TIME:
                continue

            # Trim away if one of the predicates do not match
            if not all( p( run ) for p in predicates ):
                continue

            result.append( run )

        return result
