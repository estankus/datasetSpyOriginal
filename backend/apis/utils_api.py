import math
from multiprocessing.dummy      import Pool as ThreadPool 

from ..framework.api            import Api
from ..framework.database       import local_db
from ..framework.models         import CMSRun
from ..framework.logger         import logger
from ..config                   import config
from ..errors                   import WebApiTimeoutError


@Api()
class UtilsApi:
    '''
    Utils Api
    Hosts common functions for all agents to use
    '''

    def get_processed_runs ( self, processed_by ):
        '''
        Returns runs processed by given agent
        '''
        return ( local_db
        .query(
            CMSRun )
        .filter_by(
            processed_by = processed_by )
        .all()
        )
    
    def make_async_query( self, method, args):
        '''
        Method created to make asynchronous calls to WebApi,
        because it dramatically improves performance

        params:
            method - method to call asynchronously
            args - array of arrays with params to be passed to method. 
                size of this array determines times 'method' will be called
                for example args[20][2] would mean that 'method' will be called
                20 times with 2 arguments

        return - Array of results returned by given method called with given arguments
                IMPORTANT: some result values may be None, if api timeouted
        ''' 

        self._queries_done = 0
        self._queries_failed = 0
        self._query_count = len( args )
        self._query_method = method

        # Don't know what's the best number of pools, but that api tends to be
        # quite slow, so I think bigger is better and 100 is a good number        
        pool = ThreadPool(100) 

        # Multithread calls to api
        results = pool.map(self._get_data, args)
        pool.close()
        pool.join()

        if self._queries_failed >= config.ASYNC_QUERY_FAIL_THRESHOLD:
            logger.error('Too many WebApi queries failed (%s).', config.ASYNC_QUERY_FAIL_THRESHOLD)

        return results
    
    def _get_data( self, args ):
        '''
        This method gets multithreaded. Used by make_async_query
        '''
        # To stop trying to query if there are a lot of fails
        if self._queries_failed < config.ASYNC_QUERY_FAIL_THRESHOLD:
            try:
                result = self._query_method( *args )

                self._queries_done += 1

                # Show progress nicely (print message every percent of process passes)
                if ( math.floor( self._queries_done * 100 / self._query_count ) !=
                     math.floor( ( self._queries_done - 1 ) * 100 / self._query_count )):
                    logger.info( "Async query process: %s%%", math.floor( self._queries_done * 100 / self._query_count ) )

            except WebApiTimeoutError as e:
                # Sometimes api is slow, just skip this time
                self._queries_failed += 1
                logger.warning(str(e))
                return None

        return result