'''
CMSRun Registry API

Used for relevant queries to the run registry

Tests to write:
 *  Check filter is generated correctly
 *  Check that connection works 
 *  Check results for all run classes
 *  Check filtering of tricky dates works




'''
from datetime import datetime, timedelta
import requests
import json

from ..framework import logger
from ..framework.api import Api
from ..framework.stats import Stats
from ..config import config
from ..errors import APIError
from ..libs.rhapi import RhApi, DEFAULT_URL


@Api()
class RRApi:
    '''
    Public methods:
     
     * get_runs(run_class, min_run, max_run, min_start, max_start)
        Returns run records from the run registry, filtered
        by the arguments provided.

    '''

    def __init__(self):
        
        # Newer api, that should be better supported
        self.rhApi = RhApi( config.RR_URL, debug=config.DEBUG )


    '''
        PUBLIC INTERFACE
    '''

    @Stats.timef()
    def get_runs(self,  run_class=None, min_run=None,   max_run=None,
                                        min_start=None, max_start=None ):
        '''
        Returns run records from the run registry, filtered by kwargs
        '''

        query  = 'select '
        query +=   'r.runnumber, '
        query +=   'r.starttime, '
        query +=   'r.stoptime, '
        query +=   'r.run_class_name '
        query += 'from runreg_global.runs r'

        filters, params = [], {}

        # Generate sql filters and params

        if not run_class is None:
            filters.append( 'r.run_class_name LIKE :run_class' )
            params[ 'run_class' ] = run_class

        if not min_run is None:
            filters.append( 'r.runnumber >= :min_run' )
            params[ 'min_run' ] = min_run

        if not max_run is None:
            filters.append( 'r.runnumber <= :max_run' )
            params[ 'max_run' ] = max_run

        if not min_start is None:
            filters.append( "r.starttime >= to_date( :min_start, 'YYYY-MM-DD hh24:mi:SS' )" )
            params[ 'min_start' ] = self._to_datestring( min_start )

        if not max_start is None:
            filters.append( "r.starttime <= to_date( :max_start, 'YYYY-MM-DD hh24:mi:SS' )" )
            params[ 'max_start' ] = self._to_datestring( max_start )


        if filters:
            query += ' where '
            query += ' and '.join( filters )


        try:
            logger.info( 'Calling the run registry api query="%s", params=%s', query, params )
            result = []
            for row in self.rhApi.json( query, params )[ 'data' ]:

                result.append({
                    'number'       : row[0],
                    'startTime'    : self._to_datetime( row[1] ),
                    'stopTime'     : self._to_datetime( row[2] ),
                    'runClassName' : row[3]
                })

            return result


        except Exception, e:

            try:
                msg = ': ' + e.readlines()
            except:
                msg = ''

            logger.error( 'Received error from the RhApi %s, msg %s', e, msg )
            raise APIError( 'Error when accessing the run registry %s %s' % ( e, msg ))

    '''
        PRIVATE HELPER METHODS
    '''

    def _to_datetime( self, datestring ):
        if not datestring:
            return None
        return datetime.strptime( datestring, '%Y-%m-%d %H:%M:%S' )


    def _to_datestring( self, datetime_obj ):
        return datetime_obj.strftime( '%Y-%m-%d %H:%M:%S' )
