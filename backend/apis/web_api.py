import os
from time import time
from requests.exceptions import ReadTimeout
import requests
import json

from ..framework import logger
from ..framework.api import Api
from ..framework.stats import Stats
from ..errors import APIError, WebApiTimeoutError
from ..config import config

from requests.packages.urllib3.exceptions import InsecureRequestWarning


@Api()
class WebApi:
    '''
    Web Api
    Used for relevant queries to the Tier0Api and DBSApi.

    In order to authenticate, X509_USER_PROXY environment variable needs to be set.
    It can be obtained the following way:
    # get proxy certificate
    voms-proxy-init
    # set variable
    export X509_USER_PROXY=$(voms-proxy-info --path)

    (for detailed instructions check this project wiki (if it exists hehe))
    '''

    def __init__( self ):
        self.url = config.WEB_API_URL

        # Making requests in a session reuses the same connection, thus dramatically increasing speed
        # Object of this class is created at the start of program launch and is used by all agents, 
        # so one session which will be closed when program ends
        self.session = requests.Session()
        # Certificate necessary for DBS Api
        self.session.cert = (os.getenv('X509_USER_PROXY'), os.getenv('X509_USER_PROXY'))
        # Disables annoying messages
        requests.packages.urllib3.disable_warnings( InsecureRequestWarning )


    '''
        PUBLIC INTERFACE
    '''
    @Stats.timef()
    def get_stream_config( self, run_number ):
        url = self.url + config.STREAM_CONFIG_SUFFIX
        url = self._add_args( url, run=run_number )
        return self._query_api( url )['result']

    @Stats.timef()
    def get_prompt_config( self, run_number ):
        url = self.url + config.PROMPT_CONFIG_SUFFIX
        url = self._add_args( url, run=run_number )
        return self._query_api( url )['result']

    @Stats.timef()
    def is_stream_done( self, run_number, stream ):
        url = self.url + config.STREAM_DONE_SUFFIX
        url = self._add_args( url, run=run_number, stream=stream )
        return self._query_api( url )['result'][0]

    @Stats.timef()
    def is_prompt_done( self, run_number, primary_dataset ):
        url = self.url + config.PROMPT_DONE_SUFFIX
        url = self._add_args( url, run=run_number, primary_dataset=primary_dataset )
        return self._query_api( url )['result'][0]
    
    @Stats.timef()
    def get_datasets( self, run_number, primary_ds_name = None, processed_ds_name = None, data_tier_name = None ):
        '''
        Arguments after run_number are optional
        Returns a list of dataset names
        '''
        url = self.url + config.DATASETS_SUFFIX

        args = {'run_num':run_number}
        if primary_ds_name is not None:
            args['primary_ds_name'] = primary_ds_name
        if processed_ds_name is not None:
            args['processed_ds_name'] = processed_ds_name
        if data_tier_name is not None:
            args['data_tier_name'] = data_tier_name 
        
        url = self._add_args( url, **args )

        return [ d['dataset'] for d in self._query_api( url ) ]

    @Stats.timef()
    def get_file_summaries( self, dataset, run_number ):
        url = self.url + config.FILE_SUMMARIES_SUFFIX
        url = self._add_args( url, dataset=dataset, run_num=run_number )
        return self._query_api( url )[0]


    '''
        PRIVATE HELPER METHODS
    '''

    def _add_args( self, url, **kwargs ):
        '''
        Adds given arguments to url
        '''
        for i, (name, value) in enumerate( kwargs.items() ):
            url += '?' if i == 0 else '&'
            url += '{}={}'.format(name, value)

        return url

    def _query_api( self, url, timeout=5, retry = 3 ):
        for i in range(retry):
            try:
                resp = self.session.get( url, verify=False, timeout=timeout )

                if resp.status_code == 403:
                    raise APIError( 'Authentication failed when querying API (check web_api description for details): %s' % (url) )

                return resp.json()
            
            except ReadTimeout as e:
                if i == retry - 1: 
                    raise WebApiTimeoutError( 'Connection timeout when querying API: %s ' % url )
                
                #Commented to reduce spam
                #logger.warning( 'Connection attempt %s/%s failed when querying API: %s', i+1, retry, url )

            except ValueError as e:
                raise APIError( 'Error when querying API: %s\nError: %s'  % ( url, e ))

