
from ..framework             import logger
from ..framework.agent       import Agent
from ..framework.database    import local_db
from ..framework.models      import CMSRun, Stream, Prompt, AlcaSkim
from ..framework.stats       import Stats

from ..apis.web_api          import WebApi
from ..apis.utils_api        import UtilsApi
from .cms_run_agent          import CMSRunAgent


class ConfigAgent( Agent ):
    '''
    Retrieves stream and prompt configuration data for new CMS runs
    '''

    def __init__( self ):
        Agent.__init__( self, dependencies=[UtilsApi, WebApi] )


    def agent( self, utils, webapi ):
        # Gets new runs to write stream configuration to them
        runs = utils.get_processed_runs( CMSRunAgent.__name__ )

        for i, run in enumerate( runs ):
            logger.info( 'Getting stream and prompt configs for run %s (%s/%s)', run.number, i + 1, len(runs) )

            self._getStreamConfig( run, webapi )
            self._getPromptConfig( run, webapi )
            
            run.processed_by = self.name
    
    def _getStreamConfig( self, run, webapi ):
        data = webapi.get_stream_config( run.number )
        stream_configs = []

        for entry in data:
            alca_skims = []

            if entry['alca_skim']:
                for alca_skim in entry['alca_skim'].split(','):
                    alca_skims.append( AlcaSkim( name=alca_skim ) )

            stream_configs.append( 
                Stream( 
                    run_number      = run.number, 
                    name            = entry['stream'],
                    alca_skims      = alca_skims, 
                    is_done         = False, 
                    processed_by    = self.name 
                )
            )

        run.streams = stream_configs

    def _getPromptConfig( self, run, webapi ):
        data = webapi.get_prompt_config( run.number )
        prompt_configs = []

        for entry in data:
            alca_skims = []

            if entry['alca_skim']:
                for alca_skim in entry['alca_skim'].split(','):
                    alca_skims.append( AlcaSkim( name=alca_skim ) )
            
            prompt_configs.append( 
                Prompt( 
                    run_number      = run.number, 
                    primary_dataset = entry['primary_dataset'],
                    write_aod       = entry['write_aod'], 
                    write_miniaod   = entry['write_miniaod'], 
                    alca_skims      = alca_skims, 
                    is_done         = False, 
                    processed_by    = self.name 
                )
            )

        run.prompts = prompt_configs
            