from ..framework                import logger
from ..framework.agent          import Agent
from ..framework.database       import local_db
from ..framework.models         import CMSRun, Dataset, Error
from ..framework.stats          import Stats
from ..errors                   import WebApiTimeoutError

from ..apis.web_api             import WebApi
from ..apis.utils_api           import UtilsApi
from .dataset_done_agent        import DatasetDoneAgent


class DatasetAgent( Agent ):
    '''
    Retrieves datasets for runs and checks if all expected datasets are present
    Processes runs after StreamDoneAgent
    
    Expected datasets are:
        1. /Stream(stream.name)/Run*-(stream.alca_skims.name)-*/(ALCARECO or ALCAPROMPT)
            example: /StreamCalibration/Run2017D-PromptCalibProdEcalPedestals-Express-v1/ALCAPROMPT
            NOTE: only ALCARECO are important, so only those are saved

        2. /(prompt.primary_dataset)/Run*-prompt.alca_skims.name-*/(ALCARECO or ALCAPROMPT)
            example: /Commissioning/Run2018C-TkAlMinBias-PromptReco-v3/ALCARECO
            NOTE: only ALCARECO are important, so only those are saved

        3. /(prompt.primary_dataset)/Run*/RAW
            example: /Commissioning/Run2018C-v1/RAW

        4. /(prompt.primary_dataset)/Run*/MINIAOD
            example: /Commissioning/Run2018C-PromptReco-v3/MINIAOD
            NOTE: only if prompt.write_miniaod is True

        5. /(prompt.primary_dataset)/Run*/AOD
            example: /Commissioning/Run2018C-PromptReco-v3/AOD
            NOTE: only if prompt.write_aod is True
    '''

    def __init__( self ):
        Agent.__init__( self, dependencies=[UtilsApi, WebApi] )


    def agent( self, utils, webapi ):
        runs = utils.get_processed_runs( DatasetDoneAgent.__name__ )

        logger.info("Querying API for dataset data.")
        datasets = utils.make_async_query( webapi.get_datasets, [ [x.number] for x in runs ] )

        for i, run in enumerate( runs ):
            logger.info("Processing datasets for run %s (%s/%s)", run.number, i, len(runs))

            # Api might be slow and datasets query might have timeouted
            if datasets[i]:
                for stream in run.streams:
                    # Only get datasets for newly done streams
                    if stream.processed_by == DatasetDoneAgent.__name__:
                        self._get_stream_datasets( datasets[i], run, stream )

                for prompt in run.prompts:
                    # Only get datasets for newly done prompts
                    if prompt.processed_by == DatasetDoneAgent.__name__:
                        self._get_prompt_datasets( datasets[i], run, prompt )

                run.processed_by = self.name


    def _get_stream_datasets( self, datasets, run, stream ):
        # Added 'Run' at the end, because there are some test datasets which start differently and aren't interesting to us
        beginning = '/' + 'Stream' + stream.name + '/Run'
        self._get_alca_skim_datasets( datasets, run, stream, beginning )


    def _get_prompt_datasets( self, datasets, run, prompt ):
        # Added 'Run' at the end, because there are some test datasets which start differently and aren't interesting to us
        beginning = '/' + prompt.primary_dataset + '/Run'
        self._get_alca_skim_datasets( datasets, run, prompt, beginning )

        filtered_datasets = [d for d in datasets if beginning in d]

        # RAW dataset
        dataset_name = [d for d in filtered_datasets if d.endswith('/RAW')]
        for d in dataset_name:
            dataset = self._get_dataset( d )
            dataset.runs.append( run )
        expected = beginning + '*' '/RAW'
        self._error_detection( dataset_name, run, expected )

        # AOD dataset
        if prompt.write_aod:
            dataset_name = [d for d in filtered_datasets if d.endswith('/AOD')]
            for d in dataset_name:
                dataset = self._get_dataset( d )
                dataset.runs.append( run )
            expected = beginning + '*' '/AOD'
            self._error_detection( dataset_name, run, expected )

        # MINIAOD dataset
        if prompt.write_miniaod:
            dataset_name = [d for d in filtered_datasets if d.endswith('/MINIAOD')]
            for d in dataset_name:
                dataset = self._get_dataset( d )
                dataset.runs.append( run )
            expected = beginning + '*' '/MINIAOD'
            self._error_detection( dataset_name, run, expected )


    def _get_alca_skim_datasets( self, datasets, run, streamOrPrompt, beginning):
        filtered_datasets = [d for d in datasets if beginning in d]

        for alca_skim in streamOrPrompt.alca_skims:
            alca_skim_name = '-' + alca_skim.name + '-'
            dataset_name = [d for d in filtered_datasets if alca_skim_name in d]

            for d in dataset_name:
                if d.endswith('/ALCARECO'):
                    # Note: this must be done in two lines, because otherwise when accessing runs 
                    # dataset object gets instantly garbage collected and thus cannot be updated and throws error
                    dataset = self._get_dataset( d )
                    dataset.runs.append( run )
            
            expected = beginning + '*' + alca_skim_name + '*/(ALCARECO or ALCAPROMPT)'
            self._error_detection( dataset_name, run, expected )


    def _error_detection( self, dataset_name, run, expected ):
        '''
        To be honest, these are completely useless and should be ignored, 
        because configurations lack some parameter that would tell us to not
        expect some datasets (triggers first condition here) and there are sometimes
        'updated' datasets which we also don't know when to expect (triggers second condition here)
        '''
        # If this gets triggered, that could mean the logic should be updated
        # (or small possibility is that something is actually wrong with datasets)
        if not dataset_name:
            local_db.add( Error(
                name = 'Missing dataset',
                description = "Couldn't find dataset " + expected,
                run_number = run.number) )
        # If this gets triggered, that means the logic should be updated
        elif len( dataset_name ) > 1:
            local_db.add( Error(
                name = 'Too many datasets',
                description = 'Expected to find 1 dataset, but found {}: {}'.format( len(dataset_name), dataset_name ),
                run_number = run.number) )


    def _get_dataset( self, name ):
        '''
        Returns a dataset with given name or creates a new one
        '''
        dataset = ( local_db
        .query(
            Dataset )
        .filter_by(
            name = name )
        .first() )
        
        if dataset is not None:
            return dataset
        else:
            # Split dataset into its' logical parts for convenience reasons
            # /primary_ds_name/acquisition_era-something-something/data_tier
            parts = name.split('/')

            primary_ds_name = parts[1]
            acquisition_era = parts[2].split('-')[0]
            data_tier       = parts[3]

            dataset = Dataset(  
                name            = name,
                primary_ds_name = primary_ds_name,
                acquisition_era = acquisition_era,
                data_tier       = data_tier,
                # runs must be set as an empty list, because when this dataset is returned
                # a run will be added to it
                runs            = []
            )

            local_db.add( dataset )
            return dataset