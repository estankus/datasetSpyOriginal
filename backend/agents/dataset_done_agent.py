from ..framework             import logger
from ..framework.agent       import Agent
from ..framework.database    import local_db
from ..framework.models      import CMSRun, Stream
from ..framework.stats       import Stats

from ..apis.web_api          import WebApi
from ..apis.utils_api        import UtilsApi
from .config_agent           import ConfigAgent

class DatasetDoneAgent( Agent ):
    '''
    Queries api and updates runs stream and prompt is_done status
    Processes runs after ConfigAgent
    '''

    def __init__( self ):
        Agent.__init__( self, dependencies=[UtilsApi, WebApi] )


    def agent( self, utils, webapi ):
        streamQueue = []
        promptQueue = []

        runs = self._get_unfinished_runs()

        # Get data ready for async
        for run in runs:
            for stream in run.streams:
                if not stream.is_done:
                    streamQueue.append( {'run':run, 'streamOrPrompt':stream} )

            for prompt in run.prompts:
                if not prompt.is_done:
                    promptQueue.append( {'run':run, 'streamOrPrompt':prompt} )

        logger.info("Querying api for stream done status.")
        streams = utils.make_async_query(webapi.is_stream_done, 
            [[ x['run'].number, x['streamOrPrompt'].name ] for x in streamQueue])
        
        logger.info("Querying api for prompt done status.")
        prompts = utils.make_async_query(webapi.is_prompt_done, 
            [[ x['run'].number, x['streamOrPrompt'].primary_dataset ] for x in promptQueue])
        
        for run in runs:
            # This will be unset if some dataset is not finished yet
            run.all_datasets_finished = True

        self._process_data( streamQueue, streams )
        self._process_data( promptQueue, prompts )


    def _get_unfinished_runs ( self ):
        '''
        Returns runs which not all datasets are finished
        '''
        return ( local_db
        .query(
            CMSRun )
        .filter_by(
            all_datasets_finished = False )
        .all()
        )

    def _process_data( self, queue, is_done ):
        for i, entry in enumerate ( queue ):
            if is_done[i] == None:
                # If api query fails
                entry['streamOrPrompt'].is_done = False
            else:
                entry['streamOrPrompt'].is_done = is_done[i]

            if is_done[i]:
                entry['run'].processed_by = self.name
                entry['streamOrPrompt'].processed_by = self.name
            else:
                entry['run'].all_datasets_finished = False




