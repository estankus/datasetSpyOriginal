from ..framework                import logger
from ..framework.agent          import Agent
from ..framework.database       import local_db
from ..framework.models         import DatasetRun
from ..framework.stats          import Stats

from ..apis.utils_api           import UtilsApi
from ..apis.web_api             import WebApi
from .dataset_agent             import DatasetAgent


class FileSummariesAgent( Agent ):
    '''
    Get count of events and file size for every run-dataset pair
    '''
    
    def __init__( self ):
        Agent.__init__( self, dependencies=[UtilsApi, WebApi] )

    def agent ( self, utils, webapi ):     
        runs = utils.get_processed_runs( DatasetAgent.__name__ )
        queue = []

        for run in runs:
            for dataset in run.datasets:
                datasetRun = self._get_dataset_run( dataset.id, run.number )

                # Only get data about new datasets
                if not datasetRun.num_event and not datasetRun.file_size:
                    queue.append({ 'dataset':dataset, 'run':run, 'datasetRun':datasetRun })

        logger.info("Querying API for file summaries data.")
        datasetRunInfo = utils.make_async_query( webapi.get_file_summaries, [ [x['dataset'].name, x['run'].number] for x in queue ] )

        for run in runs:
            # Assume we got all data. If not this gets reversed
            run.processed_by = self.name

        for i, entry in enumerate( queue ):
            if datasetRunInfo[i]:
                entry['datasetRun'].num_event = datasetRunInfo[i]['num_event']
                entry['datasetRun'].file_size = datasetRunInfo[i]['file_size']
            else:
                # Missing data, will have to retry next time
                entry['run'].processed_by = DatasetAgent.__name__



    def _get_dataset_run( self, dataset_id, run_number ):

        return ( local_db
        .query(
            DatasetRun )
        .filter_by(
            dataset_id=dataset_id,
            run_number=run_number )
        .first() )

            