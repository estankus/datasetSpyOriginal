from datetime import datetime, timedelta

from sqlalchemy.orm import joinedload

from ..config import config
from ..framework             import logger
from ..framework.agent       import Agent
from ..framework.database    import local_db
from ..framework.models      import RunClass, CMSRun
from ..framework.stats       import Stats
from ..apis.rr_api           import RRApi


class CMSRunAgent( Agent ):
    '''
    Responsible for providing up-to-date run information
    from the run registry and merge it into the local database.

    Logic:
      * Get all run classes and their aliases. A run class can have several
        aliases, which is handy in the event that you want to track e.g. "Cosmics17"
        and "Cosmics17CRUZET". You can then create a run class called Cosmics17
        and with two aliases on the form Cosmics17 -> [Cosmics17, Cosmic17CRUZET], or
        you can use a wildcard on the form Cosmics17 -> [Cosmics17%]. If a run class
        does not have any aliases, then the run class name will be treated as the alias.
     
     *  Purpose of the agent is to ensure local_db has all CMS runs since config.MIN_START date.
        To not have a huge query every time determine_fetching_period() is used.
     
     *  Existing run's class may change, so they are refetched for some days after their discovery
        (config.REFETCH_PERIOD) and edited if needed.

    '''

    def __init__( self ):
        Agent.__init__( self, dependencies=[RRApi] )


    def agent( self, rr_api ):
        # Get new run info for all run classes and their aliases
        for run_class in self._get_run_classes():
            self._get_new_run_data( rr_api, run_class )



    @Stats.timef()
    def _get_new_run_data( self, rr_api, run_class ):

        logger.info( 'Getting runs for run class %s', run_class.name )

        # Use run class name as alias if it does not have any aliases
        aliases = [ a.alias for a in run_class.aliases ]

        for alias in aliases or [ run_class.name ]:

            fetched_runs = self._get_new_runs( rr_api, alias )
            new_runs = []

            for entry in fetched_runs:
                existing = self._get_cms_run_by_number( entry['number'] )

                # Case: run exists in different class, so change class
                if existing and existing.run_class != run_class.name:
                    logger.info( 'Run %s changed its class from %s to %s', existing.number, existing.run_class, run_class.name )
                    existing.run_class = run_class.name
                # Case: run still hasn't got end time written
                if existing and existing.end is None:
                    existing.end = entry['stopTime']
                # Case: add new run
                elif not existing:
                    new_runs.append( entry['number'] )

                    local_db.add( CMSRun( 
                        number       = entry[ 'number' ],
                        run_class    = run_class.name,
                        start        = entry[ 'startTime' ],
                        end          = entry[ 'stopTime' ],
                        processed_by = self.name,
                        all_datasets_finished = False
                    ))
            
            logger.info( 'Added %s new runs: %s', len( new_runs ), new_runs)
          

    def _get_run_classes( self ):
        '''
        Query local db run classes
        '''
        return ( local_db
            .query(
                RunClass )
            .options(
                joinedload( RunClass.aliases ))
            .all()
        )

    
    def _get_new_runs( self, rr_api, alias ):
        '''
        Gets new runs from rr_api for given alias

        There's a 1000 run limit when fetching from rr_api, but we need all the runs,
        so this method fetches all runs from min_start date in steps
        '''
        # Number of days for one fetching
        step = 10
        new_runs = []
        min_start = self._determine_fetching_period( alias )

        # Fetch runs in steps
        while(min_start + timedelta(days = step) < datetime.now()):
            new_runs.extend( rr_api.get_runs( 
                alias,
                min_start=min_start, 
                max_start=min_start + timedelta(days = step) 
                ) 
            )

            min_start += timedelta(days = step)
        
        # Final fetch to the current date
        new_runs.extend( rr_api.get_runs( 
                alias,
                min_start=min_start, 
                ) 
            )
        
        return new_runs


    def _determine_fetching_period( self, alias ):
        '''
        Ensures correct fetching period to have all runs since config.MIN_START

        If there are no runs existing, all runs should be fetched from config.MIN_START
        If the latest run started earlier than now - config.REFETCH_PERIOD 
            runs should be fetched starting from that run
        Else runs should be fetched since now - config.REFETCH_PERIOD (because run_class may change)

        Returns date from which runs should be fetched
        '''
        refetch_from = datetime.utcnow()- timedelta(days = config.REFETCH_PERIOD)
        latest_run = self._get_latest_run( alias )
        
        if latest_run:
            if latest_run.start > refetch_from:
                logger.info( 'Fetching all runs from past %s days', config.REFETCH_PERIOD )
                return refetch_from
            logger.info( 'Fetching all runs since run %s', latest_run.number )
            return latest_run.start
        logger.info( 'Fetching all runs since %s', config.MIN_START.strftime( '%Y-%m-%d' ) )
        return config.MIN_START


    def _get_latest_run( self, alias ):
        '''
        Returns run with a highest number given run_class alias
        '''
        return ( local_db 
            .query(
                CMSRun )
            .filter(
                CMSRun.run_class.like(alias) )
            .order_by(
                CMSRun.number.desc() )
            .first()
        )


    def _get_cms_run_by_number( self, number ):
        '''
        Returns a cms run from the local database
        given run number
        '''
        return ( local_db 
            .query(
                CMSRun )
            .filter_by(
                number    = number )
            .first()
        )
