
class ErrorBase( Exception ):
    pass


class LocalDBError( ErrorBase ):
    error_type = 'LocalDBError'

class APIError( ErrorBase ):
    error_type = 'APIError'

class WebApiTimeoutError( APIError ):
    error_type = 'WebApiTimeoutError'

class AgentError( ErrorBase ):
    error_type = 'AgentError'
