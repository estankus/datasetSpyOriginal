from sqlalchemy import Column, String, Integer, DateTime, ForeignKeyConstraint, ForeignKey, Boolean, BigInteger, UniqueConstraint, Table
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


''' Common '''

class RunClassAlias( Base ):

    __tablename__  = 'RUN_CLASS_ALIAS'
    __table_args__ = (
        ForeignKeyConstraint(
            [ 'run_class' ], 
            [ 'RUN_CLASS.name' ], 
            ondelete='CASCADE' ),
        )

    run_class = Column( String( 100 ), primary_key=True ) # Name of the run class the alias is for
    alias     = Column( String( 100 ), primary_key=True ) # Alias name 

    def __str__( self ):
        return '<Run Class Alias %s => %s>' % ( self.run_class, self.alias )


class RunClass( Base ):

    __tablename__ = 'RUN_CLASS'

    name        = Column( String( 100 ), primary_key=True ) # Name of the run class

    aliases = relationship( 'RunClassAlias' )
    runs    = relationship( 'CMSRun'        )

    def __str__( self ):
        return '<Run Class %s>' % self.name


class Dataset( Base ):
    __tablename__  = 'DATASET'
    

    id              = Column( Integer, primary_key=True )
    name            = Column( String(100), unique=True )
    primary_ds_name = Column( String(100) )
    acquisition_era = Column( String(100) )
    data_tier       = Column( String(100) )      

    runs            = relationship( 'CMSRun', secondary='DATASET_RUN' )
    dataset_runs    = relationship( 'DatasetRun', back_populates='dataset' )


class DatasetRun ( Base ):
    
    __tablename__ = 'DATASET_RUN'
    __table_args__ = (
        ForeignKeyConstraint(
            [ 'dataset_id' ], 
            [ 'DATASET.id' ], 
            ondelete='CASCADE' ),
        ForeignKeyConstraint(
            [ 'run_number' ], 
            [ 'CMS_RUN.number' ], 
            ondelete='CASCADE' )
        )

    dataset_id  = Column( Integer, primary_key=True )
    run_number  = Column( Integer, primary_key=True )

    num_event   = Column( Integer )
    file_size   = Column( BigInteger )

    dataset    = relationship( 'Dataset', back_populates='dataset_runs' )
    run        = relationship( 'CMSRun', back_populates='dataset_runs' )



class CMSRun( Base ):

    __tablename__  = 'CMS_RUN'
    __table_args__ = (
        ForeignKeyConstraint(
            [ 'run_class' ], 
            [ 'RUN_CLASS.name' ], 
            ondelete='CASCADE' ),
        )

    run_class               = Column( String( 100 ) )              # PCLSpy run class this run is discovered for
    number                  = Column( Integer, primary_key=True )  # Number as reported by the run registry
    start                   = Column( DateTime, nullable=False )   # Start time as reported by the run registry
    end                     = Column( DateTime )                   # End time as reported by the run registry
    processed_by            = Column( String( 100 ), index=True )  # Which agent was the last to fully process this run
    all_datasets_finished   = Column( Boolean )                    # When this is true that means all streams and prompts are done


    streams         = relationship( 'Stream' )
    prompts         = relationship( 'Prompt' )
    datasets        = relationship( 'Dataset', secondary='DATASET_RUN' )
    dataset_runs    = relationship( 'DatasetRun', back_populates='run' )


    def __str__( self ):
        return '<CMSRun %s/%s>' % ( self.number, self.run_class )


class Stream( Base ):

    __tablename__  = 'STREAM'
    __table_args__ = (
        ForeignKeyConstraint(
            [ 'run_number' ], 
            [ 'CMS_RUN.number' ], 
            ondelete='CASCADE' ),
        )
    
    id              = Column( Integer, primary_key=True )
    run_number      = Column( Integer )
    name            = Column( String(100) )
    is_done         = Column( Boolean )                         # We can query datasets only when stream is done
    processed_by    = Column( String( 100 ), index=True )       # Which agent was the last to fully process this stream

    
    alca_skims = relationship ( 'AlcaSkim' )


class Prompt( Base ):

    __tablename__  = 'PROMPT'
    __table_args__ = (
        ForeignKeyConstraint(
            [ 'run_number' ], 
            [ 'CMS_RUN.number' ], 
            ondelete='CASCADE' ),
    )

    id              = Column( Integer, primary_key=True )
    run_number      = Column( Integer )
    primary_dataset = Column( String(100) )
    write_aod       = Column( Boolean )                     # When true we look for AOD datasets
    write_miniaod   = Column( Boolean )                     # When true we look from MINIAOD datasets
    is_done         = Column( Boolean )                     # We can query datasets only when prompt is done
    processed_by    = Column( String( 100 ), index=True )   # Which agent was the last to fully process this prompt


    alca_skims = relationship ( 'AlcaSkim' )


class AlcaSkim( Base ):

    __tablename__  = 'ALCA_SKIM'
    __table_args__ = (
        ForeignKeyConstraint(
            [ 'stream_id' ], 
            [ 'STREAM.id' ], 
            ondelete='CASCADE' ),
        ForeignKeyConstraint(
            [ 'prompt_id' ], 
            [ 'PROMPT.id' ], 
            ondelete='CASCADE' ),
        )

    id          = Column( Integer, primary_key=True )
    stream_id   = Column( Integer )
    prompt_id   = Column( Integer )
    name        = Column( String(100) )               # Will be looking for this name in dataset names


class Error ( Base ):
    __tablename__ = 'ERROR'

    id              = Column( Integer, primary_key=True )
    name            = Column( String(100) )        
    description     = Column( String(300) )   
    run_number      = Column( Integer )

''' Bookkeeping for the backend job '''


class BackendRunLog( Base ):

    __tablename__ = 'BACKEND_RUN_LOG'

    log_id  = Column( Integer, primary_key=True )
    start   = Column( DateTime, nullable=False )
    end     = Column( DateTime )


class AgentRunLog( Base ):

    __tablename__ = 'AGENT_RUN_LOG'

    agent_name = Column( String(100), primary_key=True ) 
    run_time   = Column( DateTime )


class BackendRunLogError( Base ):

    __tablename__ = 'BACKEND_RUN_LOG_ERROR'

    log_id      = Column( Integer,     ForeignKey( 'BACKEND_RUN_LOG.log_id', ondelete='CASCADE' ), 
                                       primary_key=True )
    error_no    = Column( Integer,     primary_key=True )
    agent_name  = Column( String(100), nullable=False )
    error_type  = Column( String(20),  nullable=False )
    error_text  = Column( String(400), nullable=False )
    critical    = Column( Boolean )
    filename    = Column( String( 400 ))
    lineno      = Column( Integer )
    function    = Column( String( 100 ))


    @property
    def fields( self ):
        return dict(
            agent_name = self.agent_name,
            error_type = self.error_type,
            error_text = self.error_text,
            critical   = self.critical,
            filename   = self.filename,
            lineno     = self.lineno,
            function   = self.function
        )
    

def make_tables():
    from sqlalchemy import create_engine
    from ..config import config
    engine = create_engine( config.LOCAL_CONNECTION, echo=True )
    Base.metadata.drop_all( engine )
    Base.metadata.create_all( engine )

if __name__ == '__main__':
    make_tables()
