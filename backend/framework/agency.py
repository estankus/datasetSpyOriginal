
from datetime import datetime
from time import time
import operator
import json

from .         import logger
from .database import local_db
from .models   import BackendRunLog, BackendRunLogError
from .api      import is_api_instance
from .agent    import is_agent_instance
from ..config  import config


class Agency:


    apis     = None
    agents   = None


    def __init__( self, apis=[], agents=[] ):

        not_apis = [ api for api in apis if not is_api_instance( api ) ]
        message = 'Error: the following apis are not true api instances: %s'
        assert len( not_apis ) == 0, message % not_apis
        self.apis = { api.api_class : api for api in apis }

        not_agents = [ agent for agent in agents if not is_agent_instance( agent ) ]
        message = 'Error: the following agents are not true agent instances: %s'
        assert len( not_agents ) == 0, message % not_agents
        self.agents = agents



    def run( self ):

        start = time()

        logger.info( 'Agency started %s', datetime.utcnow() )
        logger.info( 'Checking missing apis' )

        # Check that no dependencies are missing
        missing = self.get_missing_deps()
        if missing:
            logger.critical( 'Missing dependencies: \n%s', missing)
            raise RuntimeError( 'Missing dependencies' )


        # Log start and get a handle to log end later
        log_id = self.log_start()

        # Run all agents and collect their statistics
        stats = [ a.run( self.apis ) for a in self.agents ]

        # Finish up the log entry for the run
        self.log_end( log_id, stats )

        # Output statistcs if configured to
        if config.PRINT_STATS:
            delimiter1 = ' =' + ('-='*39)
            delimiter2 = ' -' + ('--'*39)
            logger.info( 'Statistics: \n%s\n\n%s\n\n%s', delimiter1, ('\n\n%s\n\n'%delimiter2).join( map( str, stats )), delimiter1 )


        nof_errors = sum( int( s.has_errors ) for s in stats )
        logger.info( 'Agency ended %s with %s errors (duration=%s sec)', datetime.utcnow(), nof_errors, time()-start )
        

    def get_missing_deps( self ):
        '''
        Get missing dependencies across all agents
        '''
        missing = []

        for a in self.agents:
            agent_missing = [d.api_class.__name__ for d in a.dependencies if not d.api_class in self.apis]
            if agent_missing:
                missing.append( ( a.name, agent_missing ) )

        return missing


    def log_start( self ):
        with local_db.connect():
            
            last_run = ( local_db 
                .query(
                    BackendRunLog )
                .order_by(
                    BackendRunLog.log_id.desc() )
                .first()
            )
            log_id = last_run.log_id + 1 if last_run else 0

            local_db.add( BackendRunLog(
                log_id = log_id,
                start  = datetime.utcnow()
            ))
            local_db.commit()

        return log_id 


    def log_end( self, log_id, stats ):

        # Concatenate errors from agent stats into one list
        errors = reduce( operator.add, [ s.errors for s in stats ], [] )

        with local_db.connect():

            entry = ( local_db 
                .query(
                    BackendRunLog )
                .filter_by(
                    log_id=log_id )
                .first()
            )
            entry.end = datetime.utcnow()

            for i, error in enumerate( errors ):
                
                local_db.session.add( BackendRunLogError(
                    log_id   = log_id,
                    error_no = i,
                    **error.fields
                ))

            local_db.commit()



    def print_configuration( self, twiki=True ):
        '''
        Prints out the current agency configuration
        '''
        print '============================================================'
        print '==========         AGENCY CONFIGURATION           =========='
        print '============================================================'
        for agent in self.agents:
            print
            agent.print_declaration( twiki )
            print
