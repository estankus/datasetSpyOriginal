from contextlib import contextmanager
import traceback

from sqlalchemy     import create_engine, event
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import DBAPIError

from .        import logger
from ..config import config
from ..errors import LocalDBError





class Database:
    '''
    Contains the bare minimum to establish a connection and doing
    the reading and writing needed by the Agency class

    Required functionality for individual Agent sub classes are
    implemented as mixins
    '''


    _session = None

    def __init__(self, uri, echo ):
        self.setUp( uri, echo )


    def setUp( self, uri, echo ):


        self.engine = create_engine(uri, echo=echo, label_length=5)
        self.Sessionmaker = sessionmaker( bind=self.engine )

        # Enable sqlite foreign keys
        if uri.startswith('sqlite'):
            logger.info( 'Add FK listener for db' )
            @event.listens_for( self.engine, 'connect' )
            def set_sqlite_pragma( conn, records ):
                cursor = conn.cursor()
                cursor.execute( 'PRAGMA foreign_keys=ON' )
                cursor.close()



    @property
    def session(self):
        '''
        Automatically raises and error if an attempt to 
        access the session attribute is made while not
        in a connected context
        '''
        if self._session is None:
            raise LocalDBError('Error: Tried to aquire session without an established connection')
        return self._session
    

    

    @contextmanager
    def connect(self, autocommit=True):
        '''
        Connects to the local database and maintains it in
        the instance's state while whithin context

        Usage:
         *  with local_db.connect():
                # Code in here will have connection

        Params:
            :autocommit: will commit session when exiting context
        '''

        # Avoid nested contexts
        if self._session is not None:
            raise LocalDBError('Error: tried to start session when one already exists')

        s = None
        try:
            # Create session and yield so the caller
            # can execute code with connection
            s = self.Sessionmaker()
            self._session = s
            yield

            # Commit if requested
            if autocommit:
                s.commit()

        except DBAPIError as e:
            logger.error(e.message)
            traceback.print_exc()
            raise LocalDBError('DBApiError within session')

        finally:
            self._session = None
            if s is not None:
                s.close()





    ''' Writing and committing '''

    @property
    def query( self ):
        return self.session.query

    @property
    def commit( self ):
        return self.session.commit

    @property
    def add( self ):
        return self.session.add

    @property
    def merge( self ):
        return self.session.merge

    @property
    def add_all( self ):
        return self.session.add_all

    @property
    def flush( self ):
        return self.session.flush

    @property
    def delete( self ):
        return self.session.delete
    


local_db = Database( config.LOCAL_CONNECTION, config.DEBUG )
