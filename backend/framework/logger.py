from functools import wraps
from contextlib import contextmanager
from threading   import local
import logging

# logging.basicConfig( level=logging.INFO, format='%(levelname)8s: asdf %(message)s' )
logger = logging.getLogger( 'backend' )

from ..config import config


_local = local()
class Prefix:

    @property
    def path( self ):
        try:
            return _local.path
        except AttributeError:
            _local.path = []
            return _local.path

    @path.setter
    def path( self, value ):
        _local.path = []

    @property
    def path_string( self ):
        try:
            return _local.path_string
        except AttributeError:
            _local.path_string = ''
            return _local.path_string

    @path_string.setter
    def path_string( self, value ):
        _local.path_string = value
    


PREFIX = Prefix()



def with_log_prefix( f ):
    '''
    Used to decorate class methods to
    insert class name as a prefix to log 
    '''
    @wraps( f )
    def decorated( self, *args, **kwargs ):
        # First argument is 'self'
        with log_prefix( self.__class__.__name__ ):
            return f( self, *args, **kwargs )

    return decorated



@contextmanager
def log_prefix( prefix ):
    try:
        add_prefix( prefix )
        yield
    finally:
        remove_prefix( prefix )
        

def add_prefix( prefix ):
    PREFIX.path.append( prefix )
    compile_prefix()


def remove_prefix( prefix ):
    while PREFIX.path:
        _prefix  = PREFIX.path.pop()
        if _prefix == prefix:
            break
    compile_prefix()


def compile_prefix():
    PREFIX.path_string = '' if not PREFIX.path else ('[ ' + ' > '.join( x for x in PREFIX.path ) + ' ]')





def debug( msg, *args ):
    if config.VERBOSE:
        logger.debug( '%s %s' % ( PREFIX.path_string, msg ), *args )

def info( msg, *args ):
    if config.VERBOSE:
        logger.info( '%s %s' % ( PREFIX.path_string, msg ), *args )

def warn( msg, *args ):
    if config.VERBOSE:
        logger.warn( '%s %s' % ( PREFIX.path_string, msg ), *args )

def warning( msg, *args ):
    if config.VERBOSE:
        logger.warn( '%s %s' % ( PREFIX.path_string, msg ), *args )

def error( msg, *args ):
    logger.error( '%s %s' % ( PREFIX.path_string, msg ), *args )

def critical( msg, *args ):
    logger.critical( '%s %s' % ( PREFIX.path_string, msg ), *args )
