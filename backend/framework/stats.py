import inspect

from contextlib  import contextmanager
from collections import defaultdict
from functools   import wraps
from time        import time
from threading   import local

from ..errors    import LocalDBError, APIError, AgentError
from .models     import BackendRunLogError



class Stats:
    '''
    Convenience class for collecting and
    outputting statisticss for agents
    '''
    
    __instances = local()
    __timed_fs  = set()
    

    ''' STATIC INTERFACE '''
   


    @staticmethod
    def has_ctx():
        '''
        Returns whether the current thread has an active stats
        context
        '''
        return hasattr( Stats.__instances, 'instance' )


    @staticmethod
    def get():
        '''
        Get current stat context, throws error if no context is active
        for current thread
        '''
        try:
            return Stats.__instances.instance
        except AttributeError:
            raise RuntimeError( 'No stats context' )


    @staticmethod
    @contextmanager
    def context( name ):
        '''
        Pushes a stat context to the current thread. Throws an error if the
        current thread already has a stat context active
        '''

        if Stats.has_ctx():
            raise RuntimeError( 'Tried to add stats context when it already exists' )

        stats = Stats( name )
        Stats.__instances.instance = stats

        try:
            yield stats 

        finally:

            stats.on_complete()
            del Stats.__instances.instance


    @staticmethod
    @contextmanager
    def time( key, strict=False ):
        '''
        Allows to time a block of code
        on key :key:, using the thread
        local stats contex
        '''
        if not strict and not Stats.has_ctx():
            yield
        else:
            stats = Stats.get()
            stats.calls[ key ].add_call()
            start = time()

            yield

            stats.calls[ key ].add_time( time() - start )


    @staticmethod
    def timef( key=None, strict=False ):
        '''
        Provides a decorator that will wrap
        the entire function in a timing block
        '''
        def decorator( f ):

            # Avoid double timing the same function
            if f in Stats.__timed_fs:
                return f
            Stats.__timed_fs.add( f )

            # Use the module/class + function name as key if none is provided
            if key:
                k = key
            else:
                module_name = f.__module__.split( '.' )[-1]
                k = '{}.{}'.format( module_name, f.__name__ )


            @wraps( f )
            def decorated( *args, **kwargs ):
                with Stats.time( k, strict=strict ):
                    return f( *args, **kwargs )

            return decorated
        return decorator




    ''' INSTANCE INTERFACE '''


    def __init__( self, name ):

        self.name     = name 
        self.calls    = defaultdict( FunctionCall )
        self.errors   = []
        self.start    = time()
        self.duration = None


    def log_error( self, e_text, e_type='Undefined', critical=False, exception=False ):
        '''
        Log an error by creating a backend log error object that can be used later
        '''
        if not exception:
            # Manual error logging, get caller info from call stack
            info = inspect.getframeinfo( inspect.stack()[1][0] )
            
            filename = info.filename
            lineno   = info.lineno
            function = info.function

        else:
            # From exception, get info from exception traceback
            info = inspect.trace()[-1]

            filename = info[1]
            lineno   = info[2]
            function = info[3]


        self.errors.append( BackendRunLogError(
            agent_name = self.name, 
            error_type = e_type,
            error_text = e_text,
            critical   = critical,
            filename   = filename,
            function   = function,
            lineno     = lineno,

        ))


    def on_complete( self ):
        '''
        Mark completed
        ''' 
        self.duration = time() - self.start


    @property
    def has_errors( self ):
        return len( self.errors ) > 0
    

    def __str__( self ):

        s =  ' {:<54}{:>25}\n'.format( self.name, 'OK!' if not self.has_errors else 'Error!'  )
        s += ' Time: {:6.2f} sec\n'.format( self.duration )

        if self.has_errors:
            s += '\n Errors:\n'
            s += '\n'.join( '   - {}:   {}'.format( e.error_type, e.error_text )
                            for e in self.errors )
            s += '\n'

        if self.calls.items():
            s += '\n Timed functions:\n' 
            s += '\n'.join( ' - {:6.2f} sec, {:4} calls, {:6.2f} sec avg, => {}'.format( 
                           stat.time,    stat.calls,     stat.avg,                 fn )
                        for fn, stat in sorted( self.calls.items(), key=lambda x: -x[1].time ))
        return s





class FunctionCall:

    def __init__( self ):
        self.calls = 0
        self.time  = 0

    def add_call( self ):
        self.calls += 1

    def add_time( self, time ):
        self.time += time

    @property
    def avg( self ):
        return float( self.time ) / self.calls
    
    @property
    def avg_str(self):
        return '{:6.2f} sec (avg)'.format( self.avg )
    

    @property
    def time_str( self ):
        return '{:6.2f} sec'.format( self.time )
    

    def __repr__( self ):
        return '<calls: %s, time: %s>' % ( self.calls, self.time_str )
