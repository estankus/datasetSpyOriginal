from datetime import datetime, timedelta
import traceback

from .              import logger
from .database      import local_db
from .models        import AgentRunLog
from .stats         import Stats
from .api           import is_api
from .logger        import with_log_prefix
from ..errors       import ErrorBase


def is_agent( cls ):
    return issubclass( cls, Agent )

def is_agent_instance( obj ):
    return isinstance( obj, Agent )


class Agent( object ):
    '''
    Abstract class for agents that takes care of tasks that
    should be done for every agent, including:

      * Connecting to the database before start and commiting
        after end

      * Adding logger context so that logger statements will
        be prefixed by the class name of the agent
      
      * Adding stats context so that functions with the Stats.timef()
        decorator will have call count and execution duration
        tracked and grouped for the agent while it runs.

    Agents extending this class must implement agent() method.
    Calling AgentWrapper().run( apis ) will call some wrapping
    code and then calls the agent() function.

    Also, implementing agents must call Agent.__init__ at the
    start of their __init__ method, as well as set their run_every
    and dependencies there if needed.
    '''

    def __init__( self, run_every=None, dependencies=[] ):
        # run_every must be timedelta
        self.run_every = run_every
        # dependencies must be APIs
        self.dependencies = dependencies
        self._agent_run_log = None
        self._last_success = None
        self.name = self.__class__.__name__
    
    # Override this method when extending this class
    def agent( self ):
        pass

    @with_log_prefix
    def run( self, apis ):
        '''
        Runs the agent by calling the run method on
        the wrapped instance
        '''
        logger.info( 'Running agent' )

        with Stats.context( self.name ) as stats:

            try:
                
                # Extract resources that should be passed to the agent
                agent_resources = [ apis[ d.api_class ].wrapped for d in self.dependencies ]

                start = datetime.utcnow()
                
                with local_db.connect():

                    if self.run_every and start < self.last_success + self.run_every:
                        # Skip if agent should only be run every now and then
                        logger.info( 'Agent last ran %s and should only be run every %s, so it is being skipped',
                                        self.last_success, self.run_every )
                    else:
                        # Else, run it like normal
                        self.agent( *agent_resources )
                    
                    self.agent_run_log.run_time = start 
                    local_db.commit()


            except ErrorBase, e:
                logger.error( '%s: %s', e.error_type, e.message )
                stats.log_error( e.message, e_type=e.error_type, critical=True, exception=True )

            except Exception, e:
                logger.error( 'Caught unexpected error %s', e.message )
                stats.log_error( e.message, e_type='Generic error', critical=True, exception=True )
                traceback.print_exc()


        logger.info( 'Done running agent' )
        return stats


    @property
    def agent_run_log( self ):
        '''
        Returns the agent run log or creates a new one if the
        agent has never run successfully before
        '''
        if self._agent_run_log is None:
            log = ( local_db
                .query( 
                    AgentRunLog )
                .filter_by(
                    agent_name = self.name )
                .first()
            )
            if log:
                self._agent_run_log = log
            else:
                self._agent_run_log = AgentRunLog( agent_name = self.name )
                local_db.add( self._agent_run_log )

        return self._agent_run_log


    @property
    def last_success( self ):
        '''
        Returns the datetime from the last time this agent
        ran successfully
        '''
        if self._last_success is None:
            self._last_success = self.agent_run_log.run_time or datetime.min
        return self._last_success


    def print_declaration( self, twiki=True ):
        '''
        Prints out the agent declaration, prints only the first paragraph
        of the description in twiki format if :twiki: is not set to False
        '''
        print '---++++ !' + self.name
        

        desc = self.__doc__ or ' -- No description --'


        if twiki:
            paragraph = desc.split( '\n\n ')[0]
            lines = [ l.strip( '\n' ).strip( ' ' ) for l in paragraph.split( '\n' ) if l ]
            print '| *Module*      |', self.__module__.replace( '.', '/' ) + '.py |'
            print '| *Dependecies* | [', ', '.join( '=%s='%d.name for d in self.dependencies), '] |'
            print '| *Description* |', ' '.join( lines ), '|'

        else:
            print '   $ *Dependecies* : [', ', '.join( '=%s='%d.name for d in self.dependencies), ']'
            print '   $ *Description* : '
            for i, paragraph in enumerate( desc.split( '\n\n' ) ):
                if i: print
                lines = [ l.strip( '\n' ).strip() for l in paragraph.split( '\n' ) ]
                print ' '.join( lines )
