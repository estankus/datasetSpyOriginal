from .         import logger
from .logger   import with_log_prefix



class ApiBase( object ):
    pass

def is_api( cls ):
    return issubclass( cls, ApiBase )

def is_api_instance( obj ):
    return isinstance( obj, ApiBase )



def Api( mocking=None, methods=None ):
    '''
    Decorator that makes makes it possible to safely 
    mock classes for testing purposes. Also wraps all 
    interface methods in the api in a logger decorator
    that makes logger statements prefixed by the api name

    Input:
      - mocking
        Api class to mock. Must also be another Api-decorated
        class if defined. Will enforce checks that this class
        implements the entire interface of the :mocking: class

      - methods
        Allows to explicitly defining the interface of the 
        decorated class. If defined, then checks will be made
        to make sure that all the methods are implemented. If
        not, then all the class' method not starting with an
        underscore are used instead.
    '''

    declared_methods = methods

    def decorator( cls ):

        class_name = cls.__name__
        interface  = []

        if mocking is not None:

            mocked_name = mocking.__name__

            # Check that api is not mocking another api and defining its own methods at the same time
            message = 'Mocking class "%s" for api "%s" can not both mock another class and have declared methods'
            assert declared_methods is None, message % ( class_name, mocked_name )

            # Check that if there is a mocking class, then then mocked class must also be an api
            message = 'Mocking class "%s" for api "%s" must also be an API'
            assert is_api( mocking ), message % ( class_name, mocked_name )

            # Check that all declared_methods of mocked class is implemented
            missing = [ m for m in mocking.interface if not hasattr( cls, m ) ]
            message = 'Mocking class "%s" for api "%s" lacks declared_methods: %s'
            assert not missing, message % ( class_name, mocked_name, missing )

        if declared_methods is not None:

            # Check that class has all declared_methods it has declared
            missing = [ m for m in declared_methods if not hasattr( cls, m ) ]
            message = 'Api class "%s" lacks declared methods: %s'
            assert not missing, message % ( class_name, missing )

            methods = declared_methods

        else:
            # Manually extract methods by getting all callables that does not start with an underscore
            methods = [ m for m in dir( cls ) if not m.startswith( '_' ) ]


        # Wrap methods in logger prefix context
        for m in methods:

            f = getattr( cls, m )

            # Ignore if not callable
            if not callable( f ):
                continue 
            
            setattr( cls, m, with_log_prefix( f ))



        class ApiWrapper( ApiBase ):

            name      = class_name
            api_class = mocking.api_class if mocking else cls
            interface = methods

            def __init__( self, *args, **kwargs ):

                self.wrapped = cls( *args, **kwargs )

                if mocking:
                    logger.info( 'Created api %s ( mocking %s )', class_name, mocking.name )
                else:
                    logger.info( 'Created api %s', class_name )


            def mock( self, **mock_values ):

                for k,v in mock_values.items():
                    setattr( self.wrapped, k, v )

                return self


        return ApiWrapper
    return decorator


