from os import path
from datetime import datetime
import json

from global_config import service


base_dir   = path.dirname( path.abspath( __file__ ))
config_dir = path.join( base_dir, 'configs/' )
cfg        = service.get_config( config_dir )


class Config:

    # General
    TESTING           = cfg.getboolean( 'GENERAL', 'testing' )
    DEBUG             = cfg.getboolean( 'GENERAL', 'debug' ) 
    VERBOSE           = cfg.getboolean( 'GENERAL', 'verbose' )
    PRINT_STATS       = cfg.getboolean( 'GENERAL', 'print_stats' ) # Whether or not statistics should be outputted

    # Local db
    LOCAL_CONNECTION = cfg.get( 'DATABASE', 'conn_string' )

    # RR api
    RR_URL  = cfg.get( 'RUN_REGISTRY', 'url' )

    # Web api
    WEB_API_URL             = cfg.get( 'WEB_API', 'url' )
    STREAM_CONFIG_SUFFIX    = cfg.get( 'WEB_API', 'stream_config_suffix' )
    PROMPT_CONFIG_SUFFIX    = cfg.get( 'WEB_API', 'prompt_config_suffix' )
    STREAM_DONE_SUFFIX      = cfg.get( 'WEB_API', 'stream_done_suffix' )
    PROMPT_DONE_SUFFIX      = cfg.get( 'WEB_API', 'prompt_done_suffix' )
    DATASETS_SUFFIX         = cfg.get( 'WEB_API', 'datasets_suffix' )
    FILE_SUMMARIES_SUFFIX   = cfg.get( 'WEB_API', 'file_summaries_suffix')

    #Agent base class
    ASYNC_QUERY_FAIL_THRESHOLD = cfg.get( 'AGENT', 'async_query_fail_threshold' )

    #CMS run agent
    MIN_START = datetime.strptime(cfg.get( 'CMS_RUN_AGENT', 'min_start' ), '%Y-%m-%d') 
    REFETCH_PERIOD = cfg.getint( 'CMS_RUN_AGENT', 'refetch_period' )


config = Config
