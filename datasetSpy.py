#!/usr/bin/env python
import sys


def main():

    # Run flask app if no arguments
    if ( len( sys.argv ) < 2 ):
        from scripts import run_flask_app
        sys.exit( run_flask_app.main() )


    script_name = sys.argv.pop( 1 )

    if script_name == 'run_backend':
        from scripts import run_backend
        sys.exit( run_backend.main() )

    # if script_name == 'run_flask_tests':
    #     from scripts import run_flask_tests
    #     sys.exit( run_flask_tests.main() )

    if script_name == 'run_backend_tests':
        from scripts import run_backend_tests
        sys.exit( run_backend_tests.main() )

    if script_name == 'clear_db':
        from scripts import clear_db
        sys.exit( clear_db.main() )

    # if script_name == 'deploy':
    #     from scripts import deploy
    #     sys.exit( deploy.main() )


    else:

        print 'Script name "%s" not recognised' % script_name
        print
        print 'Please select a script as first arg among:'
        print '  - run_backend'
        print '  - run_flask_tests'
        print '  - run_backend_tests'
        print '  - clear_db'
        print '  - deploy'

        sys.exit( 1 )






if __name__ == '__main__':
    main()
