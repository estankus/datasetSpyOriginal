'''
Flask web-application used to present the aggregated data
'''


from flask import Flask, redirect, url_for
from flask_cors import CORS
from flask_sqlalchemy   import SQLAlchemy
from sqlalchemy         import event

from .config import Config


db = SQLAlchemy()


def create_app():

    app = Flask( __name__, static_url_path='/datasetSpy/static' )
    
    app.config.from_object( Config )

    CORS ( app, resources=app.config.get( 'CORS_RESOURCES' ) )
    db.init_app ( app )

    with app.app_context():

        # Enable FKs if sqlite
        if Config.SQLALCHEMY_DATABASE_URI.startswith( 'sqlite:' ):
            @event.listens_for( db.engine, 'connect' )
            def set_sqlite_pragma( conn, records ):
                cursor = conn.cursor()
                cursor.execute( 'PRAGMA foreign_keys=ON' )
                cursor.close()

        from api import api
        app.register_blueprint( api, url_prefix='/datasetSpy/api' )




    @app.route( '/datasetSpy/app/', methods=['GET'] )
    @app.route( '/datasetSpy/app/<path:path>', methods=['GET'] )
    def index( path = None ):
        return app.send_static_file( 'app/index.html' )

    @app.route( '/datasetSpy/', methods=['GET'] )
    def frontend_redir():
        return redirect( url_for( 'index' ))

    return app
