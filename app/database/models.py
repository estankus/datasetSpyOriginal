from .. import db


'''
This file has to be synchronized with backend/framework/models.py
'''

''' Common '''

class RunClassAlias( db.Model ):

    __tablename__  = 'RUN_CLASS_ALIAS'
    __table_args__ = (
        db.ForeignKeyConstraint(
            [ 'run_class' ], 
            [ 'RUN_CLASS.name' ], 
            ondelete='CASCADE' ),
        )

    run_class = db.Column( db.String( 100 ), primary_key=True ) # Name of the run class the alias is for
    alias     = db.Column( db.String( 100 ), primary_key=True ) # Alias name 

    def __str__( self ):
        return '<Run Class Alias %s => %s>' % ( self.run_class, self.alias )


class RunClass( db.Model ):

    __tablename__ = 'RUN_CLASS'

    name        = db.Column( db.String( 100 ), primary_key=True ) # Name of the run class

    aliases = db.relationship( 'RunClassAlias' )
    runs    = db.relationship( 'CMSRun'        )

    def __str__( self ):
        return '<Run Class %s>' % self.name


class Dataset( db.Model ):
    __tablename__  = 'DATASET'
    

    id              = db.Column( db.Integer, primary_key=True )
    name            = db.Column( db.String(100), unique=True )
    primary_ds_name = db.Column( db.String(100) )
    acquisition_era = db.Column( db.String(100) )
    data_tier       = db.Column( db.String(100) ) 

    runs            = db.relationship( 'CMSRun', secondary='DATASET_RUN' )
    dataset_runs    = db.relationship( 'DatasetRun', back_populates='dataset' )


class DatasetRun ( db.Model ):
    
    __tablename__ = 'DATASET_RUN'
    __table_args__ = (
        db.ForeignKeyConstraint(
            [ 'dataset_id' ], 
            [ 'DATASET.id' ], 
            ondelete='CASCADE' ),
        db.ForeignKeyConstraint(
            [ 'run_number' ], 
            [ 'CMS_RUN.number' ], 
            ondelete='CASCADE' )
        )

    dataset_id  = db.Column( db.Integer, primary_key=True )
    run_number  = db.Column( db.Integer, primary_key=True )

    num_event   = db.Column( db.Integer )
    file_size   = db.Column( db.BigInteger )

    dataset    = db.relationship( 'Dataset', back_populates='dataset_runs' )
    run        = db.relationship( 'CMSRun', back_populates='dataset_runs' )



class CMSRun( db.Model ):

    __tablename__  = 'CMS_RUN'
    __table_args__ = (
        db.ForeignKeyConstraint(
            [ 'run_class' ], 
            [ 'RUN_CLASS.name' ], 
            ondelete='CASCADE' ),
        )

    run_class       = db.Column( db.String( 100 ) )              # PCLSpy run class this run is discovered for
    number          = db.Column( db.Integer, primary_key=True )  # Number as reported by the run registry
    start           = db.Column( db.DateTime, nullable=False )   # Start time as reported by the run registry
    end             = db.Column( db.DateTime )                   # End time as reported by the run registry
    processed_by    = db.Column( db.String( 100 ), index=True )  # Which agent was the last to fully process this run


    streams         = db.relationship( 'Stream' )
    prompts         = db.relationship( 'Prompt' )
    datasets        = db.relationship( 'Dataset', secondary='DATASET_RUN' )
    dataset_runs    = db.relationship( 'DatasetRun', back_populates='run' )


    def __str__( self ):
        return '<CMSRun %s/%s>' % ( self.number, self.run_class )


class Stream( db.Model ):

    __tablename__  = 'STREAM'
    __table_args__ = (
        db.ForeignKeyConstraint(
            [ 'run_number' ], 
            [ 'CMS_RUN.number' ], 
            ondelete='CASCADE' ),
        )
    
    id =            db.Column( db.Integer, primary_key=True )
    run_number =    db.Column( db.Integer )
    name =          db.Column( db.String(100) )
    is_done =       db.Column( db.Boolean )                       # We can query datasets only when stream is done
    processed_by =  db.Column( db.String( 100 ), index=True )   # Which agent was the last to fully process this stream
    
    alca_skims = db.relationship ( 'AlcaSkim' )


class Prompt( db.Model ):

    __tablename__  = 'PROMPT'
    __table_args__ = (
        db.ForeignKeyConstraint(
            [ 'run_number' ], 
            [ 'CMS_RUN.number' ], 
            ondelete='CASCADE' ),
    )

    id              = db.Column( db.Integer, primary_key=True )
    run_number      = db.Column( db.Integer )
    primary_dataset = db.Column( db.String(100) )
    write_aod       = db.Column( db.Boolean )                     # When true we look for AOD datasets
    write_miniaod   = db.Column( db.Boolean )                     # When true we look from MINIAOD datasets
    is_done         = db.Column( db.Boolean )                     # We can query datasets only when prompt is done
    processed_by    = db.Column( db.String( 100 ), index=True )   # Which agent was the last to fully process this prompt


    alca_skims = db.relationship ( 'AlcaSkim' )

class AlcaSkim( db.Model ):

    __tablename__  = 'ALCA_SKIM'
    __table_args__ = (
        db.ForeignKeyConstraint(
            [ 'stream_id' ], 
            [ 'STREAM.id' ], 
            ondelete='CASCADE' ),
        db.ForeignKeyConstraint(
            [ 'prompt_id' ], 
            [ 'PROMPT.id' ], 
            ondelete='CASCADE' ),
        )

    id =                db.Column( db.Integer, primary_key=True )
    stream_id =         db.Column( db.Integer )
    prompt_id =         db.Column( db.Integer )
    name =              db.Column( db.String(100) )                   # Will be looking for this name in dataset names


class Error ( db.Model ):
    __tablename__ = 'ERROR'

    id              = db.Column( db.Integer, primary_key=True )
    name            = db.Column( db.String(100) )        
    description     = db.Column( db.String(300) )   
    run_number      = db.Column( db.Integer )

''' Bookkeeping for the backend job '''


class BackendRunLog( db.Model ):

    __tablename__ = 'BACKEND_RUN_LOG'

    log_id  = db.Column( db.Integer, primary_key=True )
    start   = db.Column( db.DateTime, nullable=False )
    end     = db.Column( db.DateTime )


class AgentRunLog( db.Model ):

    __tablename__ = 'AGENT_RUN_LOG'

    agent_name = db.Column( db.String(100), primary_key=True )
    run_time   = db.Column( db.DateTime )


class BackendRunLogError( db.Model ):

    __tablename__ = 'BACKEND_RUN_LOG_ERROR'

    log_id      = db.Column( db.Integer,     db.ForeignKey( 'BACKEND_RUN_LOG.log_id', ondelete='CASCADE' ), 
                                       primary_key=True )
    error_no    = db.Column( db.Integer,     primary_key=True )
    agent_name  = db.Column( db.String(100), nullable=False )
    error_type  = db.Column( db.String(20),  nullable=False )
    error_text  = db.Column( db.String(400), nullable=False )
    critical    = db.Column( db.Boolean )
    filename    = db.Column( db.String( 400 ))
    lineno      = db.Column( db.Integer )
    function    = db.Column( db.String( 100 ))


    @property
    def fields( self ):
        return dict(
            agent_name = self.agent_name,
            error_type = self.error_type,
            error_text = self.error_text,
            critical   = self.critical,
            filename   = self.filename,
            lineno     = self.lineno,
            function   = self.function
        )
    
