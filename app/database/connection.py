'''
Database connections and how to get hold of them
'''

from functools import wraps
import logging

from flask import current_app
from sqlalchemy.orm import scoped_session
from sqlalchemy.exc import DBAPIError

from .. import db


def db_query( f ):
    ''' 
    Decorator for querying the database. Provides a
    session as the first argument to the decorated
    function.
    '''
    def decorated( *args, **kwargs ):

        # Just call as normal if nested query call
        if args and isinstance( args[0], scoped_session ):
            return f( *args, **kwargs )

        for attempt in range( current_app.config['MAX_CONNECT_ATTEMPTS'] ):
            try:
                return f( db.session, *args, **kwargs )

            except DBAPIError as e:
                current_app.logger.error( 'Database exception: %s', e )
                if e.connection_invalidated:
                    current_app.logger.info( 'Connection invalidated. Retrying...' )
                else:
                    raise e

        raise ValueError( 'Impossible to get a session after %s tries' \
                        % current_app.config['MAX_CONNECT_ATTEMPTS'] )
    return decorated

