'''
    All available queries.
    General rule: Never return ORM-object, convert to dict/list/string/etc
'''
from ..utils.timeutils import to_unix_millis

from .connection import db_query
from .models     import Dataset, RunClass

@db_query
def get_dataset_events( session, dataset, runClass ):
    dataset = (session
        .query(
            Dataset )
        .filter_by(
            name=dataset )
        .first()
        )
    
    if not dataset:
        return []

    # I don't know why, but default variable None is transformed into string
    dataset_runs = [dr for dr in dataset.dataset_runs if runClass == 'None' or runClass == None or dr.run.run_class == runClass] 

    return [ 
        { 'run':dr.run_number,
          'run_start':to_unix_millis( dr.run.start ),
          'num_event':dr.num_event }
        for dr in dataset_runs 
    ]

@db_query
def get_datasets( session ):
    datasets = (session
        .query(
            Dataset )
        .all()
        )
    
    return [
        { 'name':ds.name,
          'primary_ds_name':ds.primary_ds_name,
          'acquisition_era':ds.acquisition_era,
          'data_tier':ds.data_tier }
        for ds in datasets
    ]

@db_query
def get_run_classes( session ):
    run_classes = (session
        .query(
            RunClass )
        .all()
        )
    
    return [ { 'name':rc.name } for rc in run_classes ]