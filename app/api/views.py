from . import api
from ..database import queries



@api.route( '/test' )
def test():
    return { 'message': 'Hello World!' }


''' General API endpoints '''


@api.route( '/dataset_events' )
def get_dataset_events( dataset, run_class=None ):
    return queries.get_dataset_events( dataset, run_class )

@api.route( '/datasets' )
def get_datasets():
    return queries.get_datasets()

@api.route( '/run_classes' )
def get_run_classes():
    return queries.get_run_classes()
