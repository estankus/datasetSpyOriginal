from ..utils.api_blueprint import ApiBlueprint 

api = ApiBlueprint( 'api', __name__ )

from . import views
