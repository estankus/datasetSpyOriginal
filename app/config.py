from os import path
import json

from global_config import service


config_dir = path.join( path.dirname( path.abspath( __file__ )), 'configs/' )
cfg        = service.get_config( config_dir )


class Config:


    ''' Flask App config '''  
    ENV              = cfg.get( 'GENERAL', 'environment' )
    DEBUG            = cfg.getboolean( 'GENERAL', 'debug' ) 

    APPLICATION_NAME = service.name
    HOST             = service.hostname
    PORT             = service.listening_port

    ''' Database Config '''
    SQLALCHEMY_DATABASE_URI = service.read_connection( cfg, 'DATABASE' )
    MAX_CONNECT_ATTEMPTS    = cfg.getint( 'DATABASE', 'max_c_attempts' )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ''' CORS Config for cross domain requests '''
    CORS_RESOURCES  = json.loads(cfg.get( 'CORS', 'resources' ))




# Alias for config, this should be used when referenced elsewhere
config = Config
