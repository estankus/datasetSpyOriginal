import functools
import inspect
import json

from flask import request

from .api_error import ApiError



def with_args( *arg_defs, **opt_arg_defs ):
    '''
    Very simple request argument extractor that appends
    args in :args_defs: to the end of the functions call
    arguments and updates its keyword call arguments with
    :opt_arg_defs:
    '''
    def decorator( f ):

        @functools.wraps( f )
        def decorated( *args, **kwargs ):


            request_args = tuple( str(get_arg_or_400( a )) for a in arg_defs )
            new_args = args + request_args

            request_kwargs = dict(
                ( a, v( a, request.values.get( a )) if callable( v ) else str( request.values.get( a, v )))
                for a, v in opt_arg_defs.iteritems() )
            kwargs.update( request_kwargs )

            return f( *new_args, **kwargs )

        return decorated
    return decorator



def with_request_args( f ):
    '''
    Same as with_args except you don't have to list the
    arguments, they are instead inferred from the argument
    definition of the function
    '''
    arg_spec = inspect.getargspec( f )

    nof_defaults = len( arg_spec.defaults ) if arg_spec.defaults else 0

    # If there are kwargs present
    if arg_spec.defaults:
        nof_defaults = len( arg_spec.defaults )
        kwargs = dict( zip( reversed( arg_spec.args ), reversed( arg_spec.defaults )))
        args   = arg_spec.args[:-nof_defaults]
    else:
        kwargs = {}
        args   = arg_spec.args

    # Make decorated function
    return with_args( *args, **kwargs )( f )



def get_arg_or_400( arg_name ):
    '''
    Get arg from request or make 400 error
    '''
    arg = request.values.get( arg_name )

    if arg is None:
        raise ApiError( 'Missing arg "%s"' % arg_name, 400 )

    return arg


def with_default( default, type_name ):
    '''
    Convenience decorator for making arg extractor
    functions.
        :deafult:   default value if extracted arg is not available
        :type_name: nice string of expected arg type, used to raise error
    '''
    def decorator( f ):

        @functools.wraps( f )
        def decorated( a, v ):
            v = f( a, v )
            if not ( v is None or v == 'null' ):
                return v
            if default is not None:
                return default

            raise _wrong_arg_type( a, type_name, v )

        return decorated
    return decorator


def String( default=None, min_length=None ):
    '''
    Returns a function that takes a string
    and validates its min length
    '''
    @with_default( default, 'string' )
    def f( a, v ):

        from flask import current_app
        current_app.logger.info( '%s -> len: %s',  v, len(v ))
        if min_length and min_length > len( v ):
            raise ApiError( 'Arg "%s" must be at least %s chars long' % ( a, min_length ), 400 )

        return v

    return f


def Boolean( default=None ):
    '''
    Returns a function that extracts a boolean
    '''
    @with_default( default, 'boolean' )
    def f( a, v ):
        
        if type( v ) == bool:
            return v
        if v == 'true':
            return True
        if v == 'false':
            return False 
        if v == 'True':
            return True
        if v == 'False':
            return False

        if default is None:
            raise ApiError( 'Missing arg "%s"' % a, 400 )

    return f


def Int( default=None ):
    '''
    Returns a function that extracts an int
    '''
    @with_default( default, 'int' )
    def f( a, v ):

        if type( v ) == int:
            return v
        try:
            return int( v )
        except:
            pass
    
    return f


def Float( default=None ):
    '''
    Returns a function that extracts a float
    '''
    @with_default( default, 'float' )
    def f( a, v ):

        if type( v ) == float:
            return v
        try:
            return float( v )
        except:
            pass

    return f


def JSON( default=None ):
    '''
    Returns a function that loads json
    '''
    @with_default( default, 'json string' )
    def f( a, v ):

        try:
            return json.loads( v )
        except:
            pass

    return f


def _wrong_arg_type( name, expected, value ):
    return ApiError( 'Arg "%s" must be %s, received "(%s) %s"' % ( name, expected, type(value), value ), 400 )
