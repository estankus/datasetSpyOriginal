from functools import wraps

from flask import Blueprint, current_app, jsonify

from .request_args import with_request_args
from .api_error    import ApiError


class ApiBlueprint( Blueprint ):
    '''
    Like a normal blueprint, expect that it has
    an enhanced @route decorator that allows 
    returning a dictionary that will be formatted
    into a json response.

    It also unpacks request values corresponding to
    the function's argument signature and pass them
    to the endpoint's function
    '''

    def __init__( self, *args, **options ):

        Blueprint.__init__( self, *args, **options )

        # Add custom error handler
        self.register_error_handler( ApiError, handle_api_error )


    def route( self, rule, **options ):

        def decorator( f ):

            # Let request args be inferred from the functions arg spec
            decorated = with_request_args( f )

            # Jsonify the output of the function
            decorated = jsonify_output( decorated )

            # Add rule to blueprint's url map
            endpoint = options.pop( 'endpoint', f.__name__ )
            self.add_url_rule( rule, endpoint, decorated, **options )

            return decorated

        return decorator



def handle_api_error( error ):
    '''
    Handles errors of type ApiError
    '''
    status_code = error.status_code
    message     = error.message

    current_app.logger.error( 'API ERROR: (%s) %s', status_code, message )

    resp_dict = dict( error.payload or () )
    resp_dict[ 'message' ] = message

    resp = jsonify( resp_dict )
    resp.status_code = status_code

    return resp
    


def jsonify_output( f ):
    '''
    Decorator that jsonifies the output of a function
    '''
    @wraps( f )
    def decorated( *args, **kwargs ):
        return jsonify( f( *args, **kwargs ))

    return decorated
