from calendar import timegm
from datetime import datetime


def to_unix_millis( dateobj, default=None ):
    '''
    Returns total milliseconds since 1970
    '''
    if dateobj is None:
        if type( default ) == datetime:
            dateobj = default
        else:
            return default
    return timegm( dateobj.timetuple() ) * 1000 

def from_unix_millis( millis ):
    '''
    Returns a datetime object for millis since 1970
    '''
    milliseconds = millis or 0
    return datetime.utcfromtimestamp( milliseconds // 1000 )
