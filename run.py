#!/usr/bin/env python
'''
    Starts the flask application
'''

from scripts import run_flask_app
run_flask_app.main()